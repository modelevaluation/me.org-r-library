# PalsR Tester
Automates running PalsR with existing data on the [modelevaluation test instance](https://wwww.ncitest.modelevaluation.org). You may specify the required modeloutputs to run by ID, then parse the generated log files with a user-defined script to determine whether the commit has passed or failed. 

If all logs are checked to be successful then the commit passes to the next stage. On the master branch, the tested PalsR package will be deployed to the [modelevaluation test instance](https://www.ncitest.modelevaluation.org).

## Specifying Modeloutputs
A modeloutput uploaded to the [modelevaluation test instance](https://www.ncitest.modelevaluation.org) may be added the test process by appending it's ID to 'moIds.txt' in this directory.

This ID is obtained by clicking the copy button on the specific modeloutput's web page (located near the top right-hand corner).

## Creating a Test Script
For each of the specified modeloutputs, a log file will be generated and uniquely named - e.g. PFPewG5ZvXjtYHc34.log. These log files will be in the same directory as the 'ci/check-file.sh' script during it's runtime.

To indicate success or fail of the contents in the log file, the 'ci/check-file.sh' script should **return either 0 or 1**, respectively.
