# Master script for single site model development test
# "DEV" type analysis 
# Gab Abramowitz, UNSW, 2025 (palshelp at gmail dot com)

# First load all the packages we'll need for this analysis:
packages = c('pals','parallel','ncdf4')
lapply(packages, library, character.only = TRUE)

thisscript = 'ModelDEVsinglesite.R' # for log and error reporting
verbosity = 'on' # detail of logging: 'on' for normal info; 'debug' for verbose. 
logname = 'PALS.log' # name of log file
LogSettings = list("verbosity"=verbosity,"filename"=logname,"script"=thisscript,"chain"=thisscript)
openlog(LogSettings = LogSettings)

# Nominate variables to analyse here (use ALMA standard names) - fetches
# alternate names, units, units transformations etc:
vars = Load_VariableDetails(c('Qle','Qh','NEE'), LogSettings = LogSettings)

# Analyses that can apply to any variable:
analyses = c('Timeseries','Scatter','PDF','AnnualCycle','DiurnalCycle')

# Determine how many MOs, Benchmarks and DSs are in input files.
ModelOutputInfo = Load_FileTypes(input[["files"]],'ModelOutput', LogSettings = LogSettings)
DataSetInfo = Load_FileTypes(input[["files"]], 'DataSet', LogSettings = LogSettings)
BenchmarkInfo = Load_FileTypes(input[["files"]],'Benchmark', LogSettings = LogSettings)

# Filter datasets for a single flux tower version: ('FLUXNET2015' or 'PALS')
#DataSetInfo = FilterDataSets(DataSetInfo,keep='PALS')

requirebenchmarks = FALSE # does analysis require user to have nominated two or more benchmarks?

# Get details about different CABLE configurations to be tested, and prepare
# different configurations (in addition to other models) as independent models:
DEVmap = DEV_remapMO(ModelOutputInfo,DataSetInfo,BenchmarkInfo,LogSettings,requirebenchmarks)

outinfo = list()
# Create cluster:
cl = makeCluster((detectCores()),type='PSOCK')

# Now, create single site plot suite for each model configuration:
for(m in 1:DEVmap$MOInfo$nconfigs){ 
    ctr = 0
    # Set up analysis data and analysis list so we can use lapply or parlapply:
    AnalysisData = list()
    AnalysisList = list()
    for(v in 1:length(vars)){
        # Get first file of first DataSet:
        obs = Load_DS_MO(vars[[v]],DataSetInfo$unit[[1]]$name,
            DataSetInfo$unit[[1]]$filepaths,prop=list(propexist=FALSE),vindex=v,LogSettings = LogSettings)
        # Get all files for first model configuration:
        model = Load_DS_MO(vars[[v]],DEVmap$MOInfo$unit[[m]]$name,
            DEVmap$MOInfo$unit[[m]]$filepaths,prop=list(propexist=FALSE),vindex=v,LogSettings = LogSettings)    
        # Get all files for this configuration's benchmarks:
        bench = Load_Benchmarks(vars[[v]],BenchmarkInfo, LogSettings = LogSettings)
        # Save model, obs, bench data for each variable:
        AnalysisData[[v]] = list(obs=obs, model=model, bench = bench)
        # Add those analyses that are equally applicable to any variable to analysis list:
        for(a in 1:length(analyses)){
            ctr = ctr + 1
            AnalysisList[[ctr]] = list(vindex=v, type=paste(analyses[a],'config',m))
        }
    }
    # Process analyses list using lapply / parLapply:
    #outinfo = lapply(AnalysisList,DistributeSingleSiteAnalyses,data=AnalysisData,vars=vars)
    configplots = parLapply(cl=cl,AnalysisList,DistributeSingleSiteAnalyses,data=AnalysisData,vars=vars, LogSettings = LogSettings)
    
    # Draw summary metric table here (adds to outinfo list):
    outinfo[(length(outinfo)+1):(length(outinfo)+length(configplots)+1)] = 
        Plot_SS_MetricTable(configplots,BenchmarkInfo, LogSettings = LogSettings)
}

# stop cluster
stopCluster(cl)

# Write outinfo to output list for javascript:
output = list(files=outinfo);

closelog(LogSettings = LogSettings)
