# Simple modelevaluation.org script for checking which variables are reported in a MO
#
# Gab Abramowitz, UNSW, 2020 (palshelp at gmail dot com)

# First load all the packages we'll need for this analysis:
packages = c('pals','ncdf4')
lapply(packages, library, character.only = TRUE)

# Get alternate names, units, units transformations etc for all variables:
vars = Load_VariableDetails(c(),subset='allvars')

openlog() # create log file

# Determine details about Model Outputs in JSON file:
ModelOutputInfo = Load_FileTypes(input[["files"]],'ModelOutput')

ModelData = list() # initialise

# Load all variables in model output (1st model output is triggering MO):
for(v in 1:length(vars)){
  ModelData[[v]] = Load_DS_MO(vars[[v]],ModelOutputInfo$unit[[1]]$name,
		ModelOutputInfo$unit[[1]]$filepaths,prop=list(propexist=FALSE),vindex=v,verboseLOG=TRUE)
}

# Create plots of variable details:
outinfo = Plot_VariableTables(ModelData,vars)

# Write outinfo to output list for javascript:
output = list(files=outinfo);

closelog()
