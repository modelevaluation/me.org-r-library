PalsR
=====

Contains R libraries used by [modelevaluation.org](https://modelevaluation.org/) Analysis Scripts on the modelevaluation.org Worker.

- The libraries are in pals/
- Sample Analysis Scripts (that can be uploaded to modelevaluation.org Experiments) are in AnalysisScripts/
- Scripts and functions for the offline testbed for developing palsR are in TestScripts/

Workflow
--------

Analysis Scripts can read Data Sets (typically obs) and Model Outputs in modelevaluation.org and are sent
the following information from modelevaluation.org in the form of a JSON file:
- all Data Sets (Names and file locations) in the Experiment associated with this Analysis Script
- all Model Outputs (Names and file locations) that have been submitted to the Experiment
- the Model Output that triggered this Analysis (referred to as the Triggering Model Output)
- the Benchmarks (up to 3 other MOs in this Experiment) that the user nominated for the Triggering Model Output

The typical workflow for an Analysis Script is:
Load JSON info -> Load data -> check compatibility -> calculate metrics -> plot results -> write return JSON file

File naming convention
----------------------

File naming within palsr/pals/R roughly follows the above workflow, noting that almost all files contain a single R function:

- Load_*functionname*.R are functions associated with loading data
- Check_*functionname*.R are functions associated with checking read, grid and timing compatibility between different MOs or DSs
- Metric_*functionname*.R are functions associated with metric calculations
- Plot_*functionname*.R are functions associated with generating graphical plots

Other function groups include:
- Log_*functionname*.R (log file functions)
- Model_*functionname*.R (functions specific to the analysis of a particular model, e.g. CABLE)
- Spatial_*functionname*.R (deal with spatial transformations or calculations)
- Spread_*functionname*.R (for distributing read or analysis functions across multiple cores)
- Timing_*functionname*.R (for assessing or transforming time information)
- Units_*functionname*.R (for reading or transforming variable units)
- Utility_*functionname*.R (general utility functions for file handling)

Variable naming convention
----------------------
Variables within Data Set and Model Output netcdf files in this package are currently recognised in one of three formats:
- [ALMA netcdf](https://docs.google.com/spreadsheets/d/1CA3aTvI9piXqRqO-3MGrsH1vW-Sd87D8iZXHGrqK42o/edit#gid=2085475627)
- [CF-netcdf](https://cfconventions.org/)
- CMIP netcdf variable names

Internally, variables are referred to by their name within ALMA (CF-equivalents are in the [ALMA spreadsheet](https://docs.google.com/spreadsheets/d/1CA3aTvI9piXqRqO-3MGrsH1vW-Sd87D8iZXHGrqK42o/edit#gid=2085475627)).
