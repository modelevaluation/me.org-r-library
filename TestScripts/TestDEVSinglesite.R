# TestDEVsinglesite.R
#
# Gab Abramowitz, UNSW 2024 (gabsun at gmail dot com)
library("RJSONIO")
inputFile <- "TestInput_SingleSiteBespoke.json"
# PLUMBER2sites: 'AR-SLu', 'AT-Neu', 'AU-ASM', 'AU-Cow', 'AU-Cpr', 'AU-Ctr', 'AU-Cum', 'AU-DaP', 'AU-DaS', 'AU-Dry', 'AU-Emr', 'AU-Gin', 'AU-GWW', 'AU-How', 'AU-Lit', 'AU-Otw', 'AU-Sam', 'AU-Stp', 'AU-TTE', 'AU-Tum', 'AU-Wrr', 'AU-Ync', 'BE-Bra', 'BE-Lon', 'BE-Vie', 'BR-Sa3', 'BW-Ma1', 'CA-Qcu', 'CA-Qfo', 'CH-Cha', 'CH-Dav', 'CH-Fru', 'CH-Oe1', 'CN-Cha', 'CN-Cng', 'CN-Dan', 'CN-Din', 'CN-Du2', 'CN-HaM', 'CN-Qia', 'CZ-wet', 'DE-Bay', 'DE-Geb', 'DE-Gri', 'DE-Hai', 'DE-Kli', 'DE-Meh', 'DE-Obe', 'DE-Seh', 'DE-SfN', 'DE-Tha', 'DE-Wet', 'DK-Fou', 'DK-Lva', 'DK-Ris', 'DK-Sor', 'DK-ZaH', 'ES-ES1', 'ES-ES2', 'ES-LgS', 'ES-LMa', 'ES-VDA', 'FI-Hyy', 'FI-Kaa', 'FI-Lom', 'FI-Sod', 'FR-Fon', 'FR-Gri', 'FR-Hes', 'FR-LBr', 'FR-Lq1', 'FR-Lq2', 'FR-Pue', 'GF-Guy', 'HU-Bug', 'ID-Pag', 'IE-Ca1', 'IE-Dri', 'IT-Amp', 'IT-BCi', 'IT-CA1', 'IT-CA2', 'IT-CA3', 'IT-Col', 'IT-Cpz', 'IT-Isp', 'IT-Lav', 'IT-LMa', 'IT-Mal', 'IT-MBo', 'IT-Noe', 'IT-Non', 'IT-PT1', 'IT-Ren', 'IT-Ro1', 'IT-Ro2', 'IT-SR2', 'IT-SRo', 'JP-SMF', 'NL-Ca1', 'NL-Hor', 'NL-Loo', 'PL-wet', 'PT-Esp', 'PT-Mi1', 'PT-Mi2', 'RU-Fyo', 'SD-Dem', 'SE-Deg', 'UK-Gri', 'UK-Ham', 'US-AR1', 'US-AR2', 'US-ARM', 'US-Aud', 'US-Bar', 'US-Bkg', 'US-Blo', 'US-Bo1', 'US-Cop', 'US-FPe', 'US-GLE', 'US-Goo', 'US-Ha1', 'US-Ho1', 'US-KS2', 'US-Los', 'US-Me2', 'US-Me4', 'US-Me6', 'US-MMS', 'US-MOz', 'US-Myb', 'US-Ne1', 'US-Ne2', 'US-Ne3', 'US-NR1', 'US-PFa', 'US-Prr', 'US-SP2', 'US-SP3', 'US-SRG', 'US-SRM', 'US-Syv', 'US-Ton', 'US-Tw4', 'US-Twt', 'US-UMB', 'US-Var', 'US-WCr', 'US-Whs', 'US-Wkg', 'ZA-Kru', 'ZM-Mon'
subset = 'CABLEsinglePLUMBER2' 
sites = c('AU-How') # Just one! 
# Model to include IN ADDITION to triggering CABLE MO bundle:
# Possibilities from PLUMBER2: '1lin_raw','1lin_eb','2lin_raw', '3km27_raw','3km27_eb','6km729_raw','6km729lag_raw','RF_raw','RF_eb','LSTM_raw','LSTM_eb','PenmanMonteith','Manabe','BEPS','CABLE','CABLE-POP', 'CHTESSEL_1', 'CHTESSEL_ERA5_3', 'CHTESSEL_ERA5_4', 'CLM5', 'EntTBM','GFDL', 'JULES_GL9', 'JULES_GL9_LAI','LPJ-GUESS', 'MATSIRO','MuSICA','NoahMP','ORCHIDEE2','ORCHIDEE3','QUINCY','SDGVM','STEMMUS-SCOPE'
models = c('3km27_raw','CABLE-POP')
benchmarks = c('3km27_raw')
source('../TestScripts/TestJSONFunctions.R')
CreateSiteTestJSON(inputFile,sites,models,benchmarks,subset)
input <- fromJSON(paste(readLines(inputFile), collapse=""));
Rruntime = system.time(source("../AnalysisScripts/ModelDEVsinglesite.R"))
print(paste('Time to run:',Rruntime[3]))
output <- toJSON(output)
fileConn<-file("output_SingleSite.json")
writeLines(output, fileConn)
close(fileConn)
