# TestJSONFunctions.R
#
# Collection of functions for setting up appropriate JSON files for PALSR testing
#
# Gab Abramowitz, UNSW 2024 (gabsun at gmail dot com)
#
CreateSiteTestJSON = function(inputFile,sites,models,benchmarks,subset='PLUMBER',
	MOname='TestMO',DSname='MOtestSite'){
	files = list()
	filectr = 1
	if(subset=='PLUMBER'){
		# Add sites:
		for(s in 1:length(sites)){
			sitepath_flux=paste0('sitedata_PLUMBER/',sites[s],'Fluxnet.1.4_flux.nc')
			sitepath_met=paste0('sitedata_PLUMBER/',sites[s],'Fluxnet.1.4_met.nc')
			files[[filectr]] = list(type='DataSet',
				path=sitepath_met,mimetype='application/x-netcdf',number=as.character(s),name=paste(sites[s],'PLUMBER'),
				setId = paste0(sites[s],'_PLUMBER##n'),setModified = gsub(' ','',file.mtime(sitepath_met)))
			files[[(filectr+1)]] = list(type='DataSet',
				path=sitepath_flux,mimetype='application/x-netcdf',number=as.character(s),name=paste(sites[s],'PLUMBER'),
				setId = paste0(sites[s],'_PLUMBER##n'),setModified = gsub(' ','',file.mtime(sitepath_flux)))
			filectr = filectr + 2
		}
		bctr = 0
		# Read from OLD PALS data spreadsheet to find MO file names:
		oldPALSdata = read.csv('~/data/flux_tower/PALS/MO/PLUMBER/meta/PALS_model_output.csv')
		for(m in 1:length(models)){ # for each model
			for(s in 1:length(sites)){ # for each site for that model
				modelfilepath = paste0('modeldata_PLUMBER/',models[m],'/',models[m],'_',sites[s],'.nc')
				files[[filectr]] = list(type='ModelOutput',path=modelfilepath,
					mimetype='application/x-netcdf',number=as.character(m),name=paste0(models[m],'_PLUMBER'),
					setId = paste0(models[m],'_##n'),setModified = gsub(' ','',file.mtime(modelfilepath)))
				filectr = filectr + 1
			}
			if(any(benchmarks==models[m])){ # if this model output is a benchmark
				bctr=bctr+1
				# Write MO data again as benchmark:
				for(s in 1:length(sites)){
					benchfilepath = paste0('modeldata_PLUMBER/',models[m],'/',models[m],'_',sites[s],'.nc')
					files[[filectr]] = list(type='Benchmark',path=benchfilepath,mimetype='application/x-netcdf',
						number=as.character(bctr),name=paste(models[m],'_PLUMBER',sep=''),
						setId = paste0(models[m],'_##n'),setModified = gsub(' ','',file.mtime(benchfilepath)))
					filectr = filectr + 1
				}
			}
		}
	}else if(subset=='PLUMBER2'){
		# Single site file and single CABLE file only:
		for(s in 1:length(sites)){
			sitepath_flux=paste0('sitedata_PLUMBER2_flux/',list.files(path='./sitedata_PLUMBER2_flux',pattern=sites[s]))
			sitepath_met=paste0('sitedata_PLUMBER2_met/',list.files(path='./sitedata_PLUMBER2_met',pattern=sites[s]))
			# Add site data files:
			files[[filectr]] = list(type='DataSet',path=sitepath_flux,mimetype='application/x-netcdf',
				number=as.character(s),name=paste0(sites[s],'_PLUMBER2'),
				setId = paste0(sites[s],'_PLUMBER2##n'),setModified = gsub(' ','',file.mtime(sitepath_flux)))
			files[[(filectr+1)]] = list(type='DataSet',path=sitepath_met,mimetype='application/x-netcdf',
				number=as.character(s),name=paste0(sites[s],'_PLUMBER2'),
				setId = paste0(sites[s],'_PLUMBER2##n'),setModified = gsub(' ','',file.mtime(sitepath_met)))
			filectr = filectr + 2
		}
		bctr = 0
		for(m in 1:length(models)){
			mo_paths = list.files(path=paste0('./modeldata_PLUMBER2/',models[m]))
			for(f in 1:length(mo_paths)){
				modelfilepath = paste0('modeldata_PLUMBER2/',models[m],'/',mo_paths[f])
				files[[filectr]] = list(type='ModelOutput',path=modelfilepath,
					mimetype='application/x-netcdf',number=as.character(m),name=models[m],
					setId = paste0(models[m],'_##n'),setModified = gsub(' ','',file.mtime(modelfilepath)))
				filectr = filectr + 1
			}
			if(any(benchmarks==models[m])){ # if this model output is a benchmark
				bctr=bctr+1
				# Write MO data again as benchmark:
				for(f in 1:length(mo_paths)){
					benchfilepath = paste0('modeldata_PLUMBER2/',models[m],'/',mo_paths[f])
					files[[filectr]] = list(type='Benchmark',path=benchfilepath,
						mimetype='application/x-netcdf',number=as.character(bctr),name=models[m],
						setId = paste0(models[m],'_##n'),setModified = gsub(' ','',file.mtime(benchfilepath)))
					filectr = filectr + 1
				}
			}
		}
	}else if(subset=='UrbanPLUMBER'){
		# Single site file and single CABLE file only:
		for(s in 1:length(sites)){
			sitepath=paste0('sitedata_UrbanPLUMBER/',list.files(path='./sitedata_UrbanPLUMBER',pattern=sites[s]))
			# Add site data files:
			files[[filectr]] = list(type='DataSet',
				path=sitepath,mimetype='application/x-netcdf',number=as.character(s),
				name=paste0(sites[s],'_UrbanPLUMBER'),setId = paste0(sites[s],'_UrbanPLUMBER_##n'),
				setModified = gsub(' ','',file.mtime(sitepath)))
			filectr = filectr + 1
		}
		bctr = 0
		for(m in 1:length(models)){
			mo_paths = list.files(path=paste0('./modeldata_UrbanPLUMBER/',models[m]))
			for(f in 1:length(mo_paths)){
				modelfilepath = paste0('modeldata_UrbanPLUMBER/',models[m],'/',mo_paths[f])
				files[[filectr]] = list(type='ModelOutput',path=modelfilepath,mimetype='application/x-netcdf',
					number=as.character(m),name=paste0(models[m],'_UrbanPLUMBER'),
					setId = paste0(models[m],'Urban_##n'),setModified = gsub(' ','',file.mtime(modelfilepath)))
				filectr = filectr + 1
			}
			if(any(benchmarks==models[m])){ # if this model output is a benchmark
				bctr=bctr+1
				# Write MO data again as benchmark:
				for(f in 1:length(mo_paths)){
					benchfilepath = paste0('modeldata_UrbanPLUMBER/',models[m],'/',mo_paths[f])
					files[[filectr]] = list(type='Benchmark',path=benchfilepath,mimetype='application/x-netcdf',
						number=as.character(bctr),name=paste0(models[m],'_UrbanPLUMBER'),
						setId = paste0(models[m],'Urban_##n'),setModified = gsub(' ','',file.mtime(benchfilepath)))
					filectr = filectr + 1
				}
			}
		}
	}else if(subset=='MOtest'){
		# Just test a single MO at a single site
		files[[1]] = list(type='DataSet',
			path=sites,mimetype='application/x-netcdf',
			number=1,name=DSname,setId='setID',setModified = gsub(' ','',file.mtime(sites)))
		files[[2]] = list(type='ModelOutput',
			path=models,mimetype='application/x-netcdf',
			number=1,name=MOname,setId='setID',setModified = gsub(' ','',file.mtime(models)))
	}else if(subset=='CABLEtest_PLUMBER'){
			# testing CABLE output benchmarking in a multisite context @ PLUMBER sites
			bctr = 0
			# Add sites to JSON file:
			for(s in 1:length(sites)){
				sitepath_flux=paste0('sitedata_PLUMBER/',sites[s],'Fluxnet.1.4_flux.nc')
				sitepath_met=paste0('sitedata_PLUMBER/',sites[s],'Fluxnet.1.4_met.nc')
				files[[filectr]] = list(type='DataSet',path=sitepath_met,mimetype='application/x-netcdf',
					number=as.character(s),name=paste(sites[s],'Fluxnet1.4'),
					setId = paste0(sites[s],'_PLUMBER##n'),setModified = gsub(' ','',file.mtime(sitepath_met)))
				files[[(filectr+1)]] = list(type='DataSet',path=sitepath_flux,
					mimetype='application/x-netcdf',number=as.character(s),name=paste(sites[s],'Fluxnet1.4'),
					setId = paste0(sites[s],'_PLUMBER##n'),setModified = gsub(' ','',file.mtime(sitepath_flux)))
				filectr = filectr + 2
			}
			# Write CABLE output to JSON, and assume entire contents of CABLEoutput directory is
			# a single model output:
			runnames = dir(path='CABLEoutput/')
			mctr = 1
			for(f in 1:length(runnames)){
				mofilepath = paste0('CABLEoutput/',runnames[f])
				files[[filectr]] = list(type='ModelOutput',path=mofilepath,mimetype='application/x-netcdf',
					number=1,name=paste0('CABLE_bench_set'),setId = '_CABLE_bench_set##n',
					setModified = gsub(' ','',file.mtime(mofilepath)))
				filectr = filectr + 1
			}
			# Now write other models to JSON
			for(m in 1:length(models)){
				mctr = mctr + 1
				for(s in 1:length(sites)){
					mofilepath = paste0('modeldata_PLUMBER/',models[m],'_',sites[s],'.nc')
					files[[filectr]] = list(type='ModelOutput',path=mofilepath,mimetype='application/x-netcdf',
						number=as.character(mctr),name=paste(models[m],'_PLUMBER',sep=''),
						setId = paste0(models[m],'_PLUMBER##n'),setModified = gsub(' ','',file.mtime(mofilepath)))
					filectr = filectr + 1
				}
				if(any(benchmarks==models[m])){ # if this model output is a benchmark
					bctr=bctr+1
					# Write MO data again as benchmark:
					for(s in 1:length(sites)){
						benchfilepath = paste0('modeldata_PLUMBER/',models[m],'_',sites[s],'.nc')
						files[[filectr]] = list(type='Benchmark',
							path=benchfilepath,mimetype='application/x-netcdf',number=as.character(bctr),
							name=paste(models[m],'_PLUMBER',sep=''),setId = paste0(models[m],'_PLUMBER##n'),
							setModified = gsub(' ','',file.mtime(benchfilepath)))
						filectr = filectr + 1
					}
				}
			}
	}else if(subset=='CABLEtest_PLUMBER2'){
		# testing CABLE output benchmarking in a multisite context @ PLUMBER2 sites
		bctr = 0
		# Add sites to JSON file:
		for(s in 1:length(sites)){
			sitepath_flux=paste0('sitedata_PLUMBER2_flux/',list.files(path='sitedata_PLUMBER2_flux/',pattern=sites[s]))
			sitepath_met=paste0('sitedata_PLUMBER2_met/',list.files(path='sitedata_PLUMBER2_met/',pattern=sites[s]))
			# Add site data files:
			files[[filectr]] = list(type='DataSet',path=sitepath_flux,mimetype='application/x-netcdf',
				number=as.character(s),name=paste0(sites[s],'_PLUMBER2'),
				setId = paste0(sites[s],'_PLUMBER2##n'),setModified = gsub(' ','',file.mtime(sitepath_flux)))
			files[[(filectr+1)]] = list(type='DataSet',path=sitepath_met,mimetype='application/x-netcdf',
				number=as.character(s),name=paste0(sites[s],'_PLUMBER2'),
				setId = paste0(sites[s],'_PLUMBER2##n'),setModified = gsub(' ','',file.mtime(sitepath_met)))
			filectr = filectr + 2
		}
		# Write CABLE output to JSON, and assume entire contents of CABLEoutput directory is
		# a single model output:
		runnames = dir(path='CABLEoutput/')
		mctr = 1
		for(f in 1:length(runnames)){
			CABLEfilepath = paste0('CABLEoutput/',runnames[f])
			files[[filectr]] = list(type='ModelOutput',path=CABLEfilepath,mimetype='application/x-netcdf',
				number=as.character(1),name=paste0('CABLE_bench_set'),
				setId = 'CABLE_bench_set##n',setModified = gsub(' ','',file.mtime(CABLEfilepath)))
			filectr = filectr + 1
		}
		# Now write other models to JSON
		for(m in 1:length(models)){
			mctr = mctr + 1
			mo_paths = list.files(path=paste0('./modeldata_PLUMBER2/',models[m]))
			for(f in 1:length(mo_paths)){
				mofilepath = paste0('./modeldata_PLUMBER2/',models[m],'/',mo_paths[f])
				files[[filectr]] = list(type='ModelOutput',path=mofilepath,mimetype='application/x-netcdf',
					number=as.character(mctr),name=paste0(models[m],'_PLUMBER2'),
					setId = paste0(models[m],'_PLUMBER2##n'),setModified = gsub(' ','',file.mtime(mofilepath)))
				filectr = filectr + 1
			}
			if(any(benchmarks==models[m])){ # if this model output is a benchmark
				bctr=bctr+1
				# Write MO data again as benchmark:
				for(f in 1:length(mo_paths)){
					benchfilepath = paste0('./modeldata_PLUMBER2/',models[m],'/',mo_paths[f])
					files[[filectr]] = list(type='Benchmark',path=benchfilepath,mimetype='application/x-netcdf',
						number=as.character(bctr),name=paste0(models[m],'_PLUMBER2'),
						setId = paste0(models[m],'_PLUMBER2##n'),setModified = gsub(' ','',file.mtime(benchfilepath)))
					filectr = filectr + 1
				}
			}
		}
	}else if(subset=='CABLEsinglePLUMBER2'){
		# testing CABLE output in a single site context @ a PLUMBER2 site
		bctr = 0
		# Add sites to JSON file list:
		for(s in 1:length(sites)){
			sitepath_flux=paste0('sitedata_PLUMBER2_flux/',list.files(path='./sitedata_PLUMBER2_flux',pattern=sites[s]))
			sitepath_met=paste0('sitedata_PLUMBER2_met/',list.files(path='./sitedata_PLUMBER2_met',pattern=sites[s]))
			# Add site data files:
			files[[filectr]] = list(type='DataSet',path=sitepath_flux,mimetype='application/x-netcdf',
				number=as.character(s),name=paste0(sites[s],'_PLUMBER2'),
				setId = paste0(sites[s],'_PLUMBER2##n'),setModified = gsub(' ','',file.mtime(sitepath_flux)))
			files[[(filectr+1)]] = list(type='DataSet',path=sitepath_met,mimetype='application/x-netcdf',
				number=as.character(s),name=paste0(sites[s],'_PLUMBER2'),
				setId = paste0(sites[s],'_PLUMBER2##n'),setModified = gsub(' ','',file.mtime(sitepath_met)))
			filectr = filectr + 2
		}
		# Write CABLE output to JSON, and assume entire contents of CABLEoutput directory is
		# a single model output:
		runnames = dir(path='CABLEoutput/')
		mctr = 1
		for(f in 1:length(runnames)){
			CABLEfilepath = paste0('CABLEoutput/',runnames[f])
			files[[filectr]] = list(type='ModelOutput',path=CABLEfilepath,mimetype='application/x-netcdf',
				number=as.character(1),name=paste0('CABLE_bench_set'),
				setId = 'CABLE_bench_set##n',setModified = gsub(' ','',file.mtime(CABLEfilepath)))
			filectr = filectr + 1
		}
		# Now write other models to JSON files list:
		for(m in 1:length(models)){
			mctr = mctr + 1
			mo_paths = list.files(path=paste0('./modeldata_PLUMBER2/',models[m]))
			for(f in 1:length(mo_paths)){
				mofilepath = paste0('modeldata_PLUMBER2/',models[m],'/',mo_paths[f])
				files[[filectr]] = list(type='ModelOutput',path=mofilepath,mimetype='application/x-netcdf',
					number=as.character(mctr),name=paste0(models[m],'_PLUMBER2'),
					setId = paste0(models[m],'_PLUMBER2##n'),setModified = gsub(' ','',file.mtime(mofilepath)))
				filectr = filectr + 1
			}
			if(any(benchmarks==models[m])){ # if this model output is a benchmark
				bctr=bctr+1
				# Write MO data again as benchmark:
				for(f in 1:length(mo_paths)){
					benchfilepath = paste0('modeldata_PLUMBER2/',models[m],'/',mo_paths[f])
					files[[filectr]] = list(type='Benchmark',path=benchfilepath,mimetype='application/x-netcdf',
						number=as.character(bctr),name=paste0(models[m],'_PLUMBER2'),
					setId = paste0(models[m],'_PLUMBER2##n'),setModified = gsub(' ','',file.mtime(benchfilepath)))	
					filectr = filectr + 1
				}
			}
		}

	}
	JSONlist = list( id='mVJDHsjHSJWHK',files=files )
	write(toJSON(JSONlist),inputFile)
}

CreateGlobalTestJSON = function(inputFile,years,ds,models,benchmarks){
	files = list()
	filectr = 1
	for(y in 1:length(years)){
		for(d in 1:length(ds)){
			# Add data set:
			files[[filectr]] = list(type='DataSet',path=paste0('obsdata/',ds[d],'/',ds[d],'_v1.0_',years[y],'.nc'),
				mimetype='application/x-netcdf',number=as.character(d),name=ds[d])
			filectr = filectr + 1
		}
		# Add CABLE
		files[[filectr]] = list(type='ModelOutput',path=paste0('modeldata/CABLE/cable_out_',years[y],'.nc'),
			mimetype='application/x-netcdf',number=as.character(1),name='CABLE')
		filectr = filectr + 1
		# Add non-contiguous CABLE years as benchmark:
		files[[filectr]] = list(type='ModelOutput',
			path=paste0('modeldata/CABLE/cable_out_',(as.character(as.numeric(years[y])+1)),'.nc'),
			mimetype='application/x-netcdf',number=as.character(2),name='CABLEalt')
		filectr = filectr + 1
		files[[filectr]] = list(type='Benchmark',
			path=paste0('modeldata/CABLE/cable_out_',(as.character(as.numeric(years[y])+1)),'.nc'),
			mimetype='application/x-netcdf',number=as.character(1),name='CABLEalt')
		filectr = filectr + 1
	}

	JSONlist = list( id='mVJDHsjHSJWHK',files=files )
	write(toJSON(JSONlist),inputFile)
}
