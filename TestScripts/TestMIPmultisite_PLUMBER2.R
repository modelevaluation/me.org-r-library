# TestMIPmultisite.R
#
# Gab Abramowitz, UNSW 2024 (gabsun at gmail dot com)
library("RJSONIO")
inputFile = "TestInput_MultiSiteBespoke.json"

# sites for PLUMBER:'Amplero','Blodgett','Bugac','ElSaler','ElSaler2','Espirra','FortPeck','Harvard', 'Hesse','Howard','Howlandm','Hyytiala','Kruger','Loobos','Merbleue','Mopane','Palang','Sylvania', 'Tumba','UniMich'
#-------------------------------------------------------
# sites for PLUMBER2: 'AR-SLu', 'AT-Neu', 'AU-ASM', 'AU-Cow', 'AU-Cpr', 'AU-Ctr', 'AU-Cum', 'AU-DaP', 'AU-DaS', 'AU-Dry', 'AU-Emr', 'AU-Gin', 'AU-GWW', 'AU-How', 'AU-Lit', 'AU-Otw', 'AU-Sam', 'AU-Stp', 'AU-TTE', 'AU-Tum', 'AU-Wrr', 'AU-Ync', 'BE-Bra', 'BE-Lon', 'BE-Vie', 'BR-Sa3', 'BW-Ma1', 'CA-Qcu', 'CA-Qfo', 'CH-Cha', 'CH-Dav', 'CH-Fru', 'CH-Oe1', 'CN-Cha', 'CN-Cng', 'CN-Dan', 'CN-Din', 'CN-Du2', 'CN-HaM', 'CN-Qia', 'CZ-wet', 'DE-Bay', 'DE-Geb', 'DE-Gri', 'DE-Hai', 'DE-Kli', 'DE-Meh', 'DE-Obe', 'DE-Seh', 'DE-SfN', 'DE-Tha', 'DE-Wet', 'DK-Fou', 'DK-Lva', 'DK-Ris', 'DK-Sor', 'DK-ZaH', 'ES-ES1', 'ES-ES2', 'ES-LgS', 'ES-LMa', 'ES-VDA', 'FI-Hyy', 'FI-Kaa', 'FI-Lom', 'FI-Sod', 'FR-Fon', 'FR-Gri', 'FR-Hes', 'FR-LBr', 'FR-Lq1', 'FR-Lq2', 'FR-Pue', 'GF-Guy', 'HU-Bug', 'ID-Pag', 'IE-Ca1', 'IE-Dri', 'IT-Amp', 'IT-BCi', 'IT-CA1', 'IT-CA2', 'IT-CA3', 'IT-Col', 'IT-Cpz', 'IT-Isp', 'IT-Lav', 'IT-LMa', 'IT-Mal', 'IT-MBo', 'IT-Noe', 'IT-Non', 'IT-PT1', 'IT-Ren', 'IT-Ro1', 'IT-Ro2', 'IT-SR2', 'IT-SRo', 'JP-SMF', 'NL-Ca1', 'NL-Hor', 'NL-Loo', 'PL-wet', 'PT-Esp', 'PT-Mi1', 'PT-Mi2', 'RU-Fyo', 'SD-Dem', 'SE-Deg', 'UK-Gri', 'UK-Ham', 'US-AR1', 'US-AR2', 'US-ARM', 'US-Aud', 'US-Bar', 'US-Bkg', 'US-Blo', 'US-Bo1', 'US-Cop', 'US-FPe', 'US-GLE', 'US-Goo', 'US-Ha1', 'US-Ho1', 'US-KS2', 'US-Los', 'US-Me2', 'US-Me4', 'US-Me6', 'US-MMS', 'US-MOz', 'US-Myb', 'US-Ne1', 'US-Ne2', 'US-Ne3', 'US-NR1', 'US-PFa', 'US-Prr', 'US-SP2', 'US-SP3', 'US-SRG', 'US-SRM', 'US-Syv', 'US-Ton', 'US-Tw4', 'US-Twt', 'US-UMB', 'US-Var', 'US-WCr', 'US-Whs', 'US-Wkg', 'ZA-Kru', 'ZM-Mon'
# removed sites (precip issues): 'AU-Rig', 'AU-Rob','AU-Whr','CA-NS1', 'CA-NS2', 'CA-NS4', 'CA-NS5', 'CA-NS6', 'CA-NS7','CA-SF1', 'CA-SF2', 'CA-SF3', 'RU-Che', 'RU-Zot','UK-PL3', 'US-SP1'
#-------------------------------------------------------
# sites for UrbanPLUMBER: 'AU-Preston', 'JP-Yoyogi','SG-TelokKurau','AU-SurreyHills','KR-Jungnang','UK-KingsCollege','CA-Sunset','KR-Ochang','UK-Swindon','FI-Kumpula','MX-Escandon','US-Baltimore','FI-Torni','NL-Amsterdam','US-Minneapolis1','FR-Capitole','PL-Lipowa','US-Minneapolis2','GR-HECKOR','PL-Narutowicza','US-WestPhoenix'
#-------------------------------------------------------
# Models for UrbanPLUMBER: "ASLUM2_b","ASLUM2_d","ASLUM3.1_b","ASLUM3.1_d","BEPCOL_b","BEPCOL_d","CABLE","CHTESSEL_CTL","CHTESSEL_URB","CHTESSEL_URB_DET","CLMU5_b","CLMU5_d","CM","CM-BEM_d","JULES_1T_d","JULES_2T_d","JULES_MORUSES_d","K-UCM_d","Lodz_b","Lodz_d","Manabe_1T_d","Manabe_2T_d","NOAH_SLAB","NOAH_SLUCM_b","NOAH_SLUCM_d","REG1","SNUUCM_b","SNUUCM_d","SUEWS_b","SUEWS_d","TARGET","TEB-CNRM_b","TEB-CNRM_d","TEB-SPARTACUS_b","TEB-SPARTACUS_d","TEB4_b","TEB4_d","TERRA4_b","TERRA4_d","UCLEM","UTC_b","UTC_d","UTC_M_b","UTC_M_d","UTC_M_NoI_d","UTC_NoI_d","VTUF"
#-------------------------------------------------------
# Models for PLUMBER: '1lin','2lin','3km27','CABLE.2.0','CABLE_2.0_SLI.vxh599_r553','Penman_Monteith.1','Manabe_Bucket.1', 'ISBA_SURFEX_dif.SURFEX7.3','ORCHIDEE.trunk_r1401','JULES.3.1','Noah.3.2','Mosaic.1','Noah.2.7.1','NOAH.3.3'
#-------------------------------------------------------
# Models for PLUMBER2: '1lin_raw','1lin_eb','2lin_raw', '3km27_raw','3km27_eb','6km729_raw','6km729lag_raw','RF_raw','RF_eb','LSTM_raw','LSTM_eb','PenmanMonteith','Manabe','BEPS','CABLE','CABLE-POP', 'CHTESSEL_1', 'CHTESSEL_ERA5_3', 'CHTESSEL_ERA5_4', 'CLM5', 'EntTBM','GFDL', 'JULES_GL9', 'JULES_GL9_LAI','LPJ-GUESS', 'MATSIRO','MuSICA','NoahMP','ORCHIDEE2','ORCHIDEE3','QUINCY','SDGVM','STEMMUS-SCOPE'
#-------------------------------------------------------

subset = 'PLUMBER2' # 'PLUMBER' or 'PLUMBER2' or 'UrbanPLUMBER'
sites = c('AR-SLu', 'AT-Neu', 'AU-ASM', 'AU-Cow', 'AU-Cpr', 'AU-Ctr', 'AU-Cum', 'AU-DaP', 'AU-DaS', 'AU-Dry', 'AU-Emr', 'AU-Gin', 'AU-GWW', 'AU-How', 'AU-Lit', 'AU-Otw', 'AU-Sam', 'AU-Stp', 'AU-TTE', 'AU-Tum', 'AU-Wrr', 'AU-Ync', 'BE-Bra', 'BE-Lon', 'BE-Vie', 'BR-Sa3', 'BW-Ma1', 'CA-Qcu', 'CA-Qfo', 'CH-Cha', 'CH-Dav', 'CH-Fru', 'CH-Oe1', 'CN-Cha', 'CN-Cng', 'CN-Dan', 'CN-Din', 'CN-Du2', 'CN-HaM', 'CN-Qia', 'CZ-wet', 'DE-Bay', 'DE-Geb', 'DE-Gri', 'DE-Hai', 'DE-Kli', 'DE-Meh', 'DE-Obe', 'DE-Seh', 'DE-SfN', 'DE-Tha', 'DE-Wet', 'DK-Fou', 'DK-Lva', 'DK-Ris', 'DK-Sor', 'DK-ZaH', 'ES-ES1', 'ES-ES2', 'ES-LgS', 'ES-LMa', 'ES-VDA', 'FI-Hyy', 'FI-Kaa', 'FI-Lom', 'FI-Sod', 'FR-Fon', 'FR-Gri', 'FR-Hes', 'FR-LBr', 'FR-Lq1', 'FR-Lq2', 'FR-Pue', 'GF-Guy', 'HU-Bug', 'ID-Pag', 'IE-Ca1', 'IE-Dri', 'IT-Amp', 'IT-BCi', 'IT-CA1', 'IT-CA2', 'IT-CA3', 'IT-Col', 'IT-Cpz', 'IT-Isp', 'IT-Lav', 'IT-LMa', 'IT-Mal', 'IT-MBo', 'IT-Noe', 'IT-Non', 'IT-PT1', 'IT-Ren', 'IT-Ro1', 'IT-Ro2', 'IT-SR2', 'IT-SRo', 'JP-SMF', 'NL-Ca1', 'NL-Hor', 'NL-Loo', 'PL-wet', 'PT-Esp', 'PT-Mi1', 'PT-Mi2', 'RU-Fyo', 'SD-Dem', 'SE-Deg', 'UK-Gri', 'UK-Ham', 'US-AR1', 'US-AR2', 'US-ARM', 'US-Aud', 'US-Bar', 'US-Bkg', 'US-Blo', 'US-Bo1', 'US-Cop', 'US-FPe', 'US-GLE', 'US-Goo', 'US-Ha1', 'US-Ho1', 'US-KS2', 'US-Los', 'US-Me2', 'US-Me4', 'US-Me6', 'US-MMS', 'US-MOz', 'US-Myb', 'US-Ne1', 'US-Ne2', 'US-Ne3', 'US-NR1', 'US-PFa', 'US-Prr', 'US-SP2', 'US-SP3', 'US-SRG', 'US-SRM', 'US-Syv', 'US-Ton', 'US-Tw4', 'US-Twt', 'US-UMB', 'US-Var', 'US-WCr', 'US-Whs', 'US-Wkg', 'ZA-Kru', 'ZM-Mon')
models = c('1lin_raw','1lin_eb','2lin_raw', '3km27_raw','3km27_eb','6km729_raw','6km729lag_raw','RF_raw','RF_eb','LSTM_raw','LSTM_eb','PenmanMonteith','Manabe','BEPS','CABLE','CABLE-POP', 'CHTESSEL_1', 'CHTESSEL_ERA5_3', 'CHTESSEL_ERA5_4', 'CLM5', 'EntTBM','GFDL', 'JULES_GL9', 'JULES_GL9_LAI','LPJ-GUESS', 'MATSIRO','MuSICA','NoahMP','ORCHIDEE2','ORCHIDEE3','QUINCY','SDGVM','STEMMUS-SCOPE') # complete list of models in Experiment
benchmarks = c('1lin_raw','3km27_raw','LSTM_raw') # user-nominated benchmarks, as though in modelevaluation.org
source('../TestScripts/TestJSONFunctions.R')
CreateSiteTestJSON(inputFile,sites,models,benchmarks,subset)

# Run the multisite experiment:
input <- fromJSON(paste(readLines(inputFile), collapse=""));
Rruntime = system.time(source('../AnalysisScripts/PLUMBER2_MIP.R'))
print(paste('Time to run:',Rruntime[3]))
warnings()
output <- toJSON(output)
fileConn<-file("output_MultiSite.json")
writeLines(output, fileConn)
close(fileConn)
