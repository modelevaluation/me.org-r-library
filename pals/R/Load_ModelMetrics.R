# Load_ModelMetrics.R
#
# Gab Abramowitz UNSW 2024 (gabsun at gmail dot com)
#
# This function is called for a single model-site in a multi-site experiment.
# It loads model data (including any requested conversions for later analyses),
# then for each variable, calculates each requested metric. If the model being
# loaded has a longer time step size than obs, obs will be averaged to be the same.
# refInterval will force them both to match a particular time step size.

Load_ModelMetrics = function(modelinfo,refInterval,vars,conversions,
	metrictypes,makespace=TRUE,LogSettings){

      writelog(paste(' Begin Load_ModelMetrics for',modelinfo$obs[[1]]$name,'for Model Output',modelinfo$name), level = "debug", LogSettings)
	# Note which site it is that we want to load from this multi-site model output
	# (nb subsetid timing, global attribute properties taken from first variable only):
	subsetidentifier = list(type='fluxtower',name=modelinfo$obs$name,
		grid=modelinfo$obs[[1]]$grid,timing=modelinfo$obs[[1]]$timing,
		globalatts=modelinfo$obs[[1]]$globalatts)

	# Load model data for this site (using subset identifier to select site from files):
	model = Load_Site(modelinfo,refInterval,vars,conversions,
		subsetidentifier=subsetidentifier,forceqc=modelinfo$obs, LogSettings = LogSettings)

	# Now calculate all requested metrics for each requested variable:
	for(v in 1:length(vars)){ # for each variable

		model[[v]]$metricerr = FALSE # initialise as no errors

		# Check that both obs and model loaded okay (before we calculate metrics):
		readcheck = CheckDataRead(modelinfo$obs[[v]]$err,modelinfo$obs[[v]]$errtext,
			model[[v]]$err,model[[v]]$errtext, LogSettings = LogSettings)

		if(readcheck$err | all(is.na(model[[v]]$data)) ){ # MO or obs failed, cancel metrics for this MO-site
			errtext = paste0(' !! Site: ',modelinfo$obs[[v]]$name,', Model: ',
				model[[v]]$name,', variable: ',vars[[v]]$Name[1],' failed. !! ',readcheck$errtext)
			writelog(errtext, level = "warn", LogSettings) # Write this info to log file if verbose
			model[[v]]$metricerr = TRUE
			model[[v]]$metrics = c(1:length(metrictypes))*NA # fill with NAs
			next # move onto the next variable in this for loop
		}else{ # obs and MO loaded fine
			# Check if this MO has different time step size to obs (note if MO is the triggering
			# MO or one of its benchmarks obs will already have been converted when loading):
			if(modelinfo$obs[[v]]$timing$interval != model[[v]]$timing$interval){
				# Overwrite obs variable data with new time step size data (only within this function)
				# (write 1st list item returned, since we're only sending one to be changed):
				modelinfo$obs[[v]] = TimeHomogenise(model[[v]]$timing$interval,changedata=list(modelinfo$obs[[v]]), LogSettings = LogSettings)[[1]]
			}
		}

		# Check obs and model vars/timing/grids are compatible:
		compatcheck = CanAnalysisProceed(modelinfo$obs[[v]],model[[v]], LogSettings = LogSettings)
		if(compatcheck$err){
			errtext = paste0(' !! Site: ',modelinfo$obs[[v]]$name,', ',vars[[v]]$Name[1],
				', Model: ',model[[v]]$name,' failed. !! ',compatcheck$errtext)
			writelog(errtext, level = "warn", LogSettings) # Write to log file:
			model[[v]]$metricerr = TRUE
			model[[v]]$metrics = c(1:length(metrictypes))*NA # fill with NAs
			next # move onto the next variable in this for loop
		}

		# Add obs quality control info, if present:
		if(modelinfo$obs[[v]]$qcexists){
			# Replace non-good qc obs values with NA, for use by metric calculations:
			modelinfo$obs[[v]]$data =
				ifelse( (modelinfo$obs[[v]]$qc %in% modelinfo$obs[[v]]$goodqcvalue),modelinfo$obs[[v]]$data, NA)
		}

		# Now calculate all requested metrics for this variable:
		writelog(paste(' Calculate',vars[[v]]$Name[1], 'metrics for',modelinfo$obs[[v]]$name,
			'for Model Output',model[[1]]$name), level = "debug", LogSettings)
		metricvalues = c()
		for(mt in 1:length(metrictypes)){
			# Note that 'get' means metrictypes[mt] becomes a function name:
			metricvalues[mt] = get(metrictypes[mt])(model[[v]]$data,modelinfo$obs[[v]]$data, LogSettings = LogSettings)
		}

		# Add metric values to model data:
		model[[v]]$metrics = metricvalues

		# If requested, remove model data now metrics have been calculated (excepting any)
		# requested conversions (clears memory for larger analyses):
		if(makespace){
			# data and qc are almost all of the space used:
			model[[v]]$data = NA
			model[[v]]$qcexists = FALSE
			model[[v]]$qc = NA
			gc() # uncomment to drop memory usage - but slower
		}
	} # over each variable

	# Save model name:
	model$name = modelinfo$name

	# Write to log file:
	writelog(paste(' Analysis at',modelinfo$obs[[v]]$name,'for Model Output',model[[1]]$name,'is complete.'), level = "info", LogSettings)

	# Return result:
	return( model )
}
