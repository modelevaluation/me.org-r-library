# Metric_PDFoverlap.R
#
# Gab Abramowitz UNSW 2023 (gabsun at gmail dot com)
#
PDFoverlap = function(model,obs, LogSettings){
	# Number of data we want before trying to calculate a density (defined in constants.R)
  	minsample = density_minsample 
  	# Number of bins in density estimates (defined in constants.R)   
  	nbins=density_nbins 
	# Create obs mask to make sure PDF overlap is calculated over the same time steps for obs and MO
	NotNAmask = ! ( is.na(as.vector(obs)) | is.na(as.vector(model)) )
	if(sum(NotNAmask)<minsample){ # threshold for PDFoverlap to mean something
		PDFoverlap = NA
	}else{
		xmax = max(as.vector(model)[NotNAmask],as.vector(obs)[NotNAmask])
		xmin = min(as.vector(model)[NotNAmask],as.vector(obs)[NotNAmask])
		modden = density(as.vector(model)[NotNAmask],from=xmin,to=xmax,n=nbins)
		obsden = density(as.vector(obs)[NotNAmask],from=xmin,to=xmax,n=nbins)
		# Calculate overlap with observational pdf:
		intersection=c()
		for(b in 1:nbins){
			intersection[b]=min(modden[[2]][b],obsden[[2]][b])
		}
		PDFoverlap = min(signif(sum(intersection)*(xmax-xmin)/nbins*100,2),100) # as a percentage
	}
	return(PDFoverlap)
}
