# DEV_LoadDetails.R
#
# Gab Abramowitz UNSW 2024 (gabsun at gmail dot com)
#
# Get properties of DEV LSM (LSM in benchmarking bundle MO) revisions and configurations.
# The LSM benchmarking bundle (triggering MO) will have:
# nconfigs configurations x
# nrevisions revisions x
# nsite files

DEV_LoadDetails = function(ModelOutputInfo,DataSetInfo, LogSettings){

	thisfunction = 'DEV_LoadDetails.R'
	rev = list()
	revctr = 0
	nrevisions=1
	nsites_inExpt = length(DataSetInfo$unit) # number of tower sites in meorg experiment
	for(mo in 1:ModelOutputInfo$number){
		if(ModelOutputInfo$unit[[mo]]$number==1){
			# i.e. this is the triggering MO; if not, we're not doing LSM benchmarking
			# First, get global attributes for each file:
			globalatts = DEV_GlobalAtts(ModelOutputInfo$unit[[mo]]$filepaths, LogSettings)
			# Make sure all files have 'benchcab_version" attribute:
			if(! all(globalatts$isBenchcab)){
				# If not, crash gracefully:
				message = paste0('This experiment expects to analyse a LSM benchmarking ',
					'bundle with multiple configurations and revisions... did not recognise',
					' your Model Output as a LSM bundle. ',thisfunction)
				writelog(message, level = "error", LogSettings)
				stop(message)
			}
			# Make sure all files have 'filename%out' attribute:
			if(! all(globalatts$hasFileout)){
				# If not, crash gracefully:
				message = paste0('All files in LSM benchmarking bundle (triggering model output)',
					' need to have filename%out global attribute to determine configurations ',
					'and revisions. ', thisfunction)
				writelog(message, level = "error", LogSettings)
				stop(message)
			}
			# Find locations of Revision and Science configuration information within filenames. This uses
			# global attributes to do this since meorg will likely change file names in object store:
			rstarts = regexpr('_R',globalatts$fileouts)
			sstarts = regexpr('_S',globalatts$fileouts)
			ostarts = regexpr('_out.nc',globalatts$fileouts)
			# Found the triggering LSM MO bundle:
			BundleIndex = mo # save which meorg MO is the LSM bundle
			paths = globalatts$fileouts
			# Find the DEV LSM revision number for each file/run (will be duplicates):
			revisionnums = as.integer(substr(paths,start=(rstarts+2),stop=(sstarts-1)))
			revisions = unique(revisionnums) # no duplicates
			nrevisions = length(revisions) # include R0 (trunk) as a revision
			# Find DEV LSM configuration numbers of each file (will be duplicates):
			confignums = as.integer(substr(paths,start=(sstarts+2),stop=(ostarts-1)))
			configs = unique(confignums) # no duplicates
			nconfigs = length(configs) # will include S0 case
			DEVLSMsites = length(paths) / nconfigs / nrevisions # number of tower sites in the DEV LSM bundle
			# Store index of revision and config numbers amongst files:
			revconffileidx = array(NA,dim=c(nrevisions,nconfigs,DEVLSMsites))
			sitectr = matrix(0,nrevisions,nconfigs) # counts number of sites recorded for rev, config
			for(f in 1:length(paths)){
				# increment counter for number of sites recorded for this rev, config. Note "+1" is because
				# of R0/C0 convention, and we want array indexing starting at 1:
				sitectr[(revisionnums[f]+1),(confignums[f]+1)] = sitectr[(revisionnums[f]+1),(confignums[f]+1)] + 1
				revconffileidx[(revisionnums[f]+1),(confignums[f]+1),sitectr[(revisionnums[f]+1),(confignums[f]+1)]] = f
			}
			# Report details to log file:
			logseparator(LogSettings)
			writelog(paste0('Triggering ModelOutput is a model development bundle with ',nrevisions,
				' revisions and ',nconfigs,' configurations, each with ',DEVLSMsites,' sites.'),
				level = "info", LogSettings)
			if(DEVLSMsites != nsites_inExpt){
				writelog(paste0('NOTE:  model development bundle contains ',DEVLSMsites,
					' sites, whereas modelevaluation.org experiment contains ',nsites_inExpt,'.'),
					level = "info", LogSettings)
			}
			# Return and stop searching MOs, since we found the DEV LSM MO set
			return(list(found=TRUE,fileidx=revconffileidx,MOidx=BundleIndex,nsites=DEVLSMsites,
				nrevisions=nrevisions,nconfigs=nconfigs,revisions=revisions,configs=configs))
		} # if triggering MO
	}
	# If we didn't find one, return with error message
	msg = paste('No development LSM fileset bundle found to benchmark.',thisfunction)
	writelog(msg,level = "error",LogSettings)
	
	return(list(found=FALSE,msg=msg))
}
