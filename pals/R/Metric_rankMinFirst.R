# Metric_rankMinFirst.R
#
# Gab Abramowitz UNSW 2023 (gabsun at gmail dot com)
#
# Rank different values of the same metric.
# For metrics where lowest value is best (e.g. RMSE, NME, MBE, SDdiff,skewdiff,kurtosisdiff)

rankMinFirst = function(metricvals,metricrange=FALSE,benchrangeonly=TRUE, LogSettings){
	# First, if any metric values are NA - i.e. analysis failed, set ranks to NA
	if(any(is.na(metricvals)) | any(is.infinite(metricvals))){
		rankMinFirst = rep(c(NA),times=length(metricvals))
	}else if(metricrange & benchrangeonly){
		benchmetrics = metricvals[ 2:length(metricvals) ]
		mrange = max(benchmetrics) - min(benchmetrics) # range of metrics
		distfrommin = metricvals - min( benchmetrics ) # vector of distances from minimum value
		rankMinFirst = distfrommin / mrange # scale by range
	}else if(metricrange & !benchrangeonly){
		mrange = max(metricvals) - min(metricvals) # range of metrics
		distfrommin = metricvals - min(metricvals) # vector of distances from minimum value
		rankMinFirst = distfrommin / mrange # scale these to get value [0,1]
	}else{
		rankMinFirst = rank(metricvals,ties.method = 'min')
	}
	return(rankMinFirst)
}
