# Timing_DayNight.R
#
# Gab Abramowitz UNSW 2020 (gabsun at gmail dot com)
#
# Returns a logical day/night time series based on a SW threshold

DayNight = function(SWdown,threshold = 5,LogSettings){
	daynotnight = c()
	daynotnight[1:length(SWdown)] = FALSE
	for(t in 1:length(SWdown)){
		if(SWdown[t]>threshold){
			daynotnight[t]=TRUE
		}
	}
	return(daynotnight)
}
