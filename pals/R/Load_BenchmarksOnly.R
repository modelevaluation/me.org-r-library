# Load_BenchmarksOnly.R
#
# Function for reducing model outputs to only include the triggering MO and benchmarks
#
# Gab Abramowitz, UNSW, 2021 (palshelp at gmail dot com)

Load_BenchmarksOnly = function(ModelOutputInfo,BenchmarkInfo, LogSettings){

	trigMOfiles = length(ModelOutputInfo$unit[[1]]$filepaths) # number of files in triggering MO
	nMOfiles = trigMOfiles + BenchmarkInfo$nfiles # number of files in new (reduced) MO list

	MOunits = list()
	MOunits[[1]] = ModelOutputInfo$unit[[1]] # First MO is triggering MO

 	# First part of files list are those files belonging to the triggering MO:
	MOfiles = list()
	for(f in 1:trigMOfiles){
		MOfiles[[f]] = list(path=ModelOutputInfo$unit[[1]]$filepaths[f],name=ModelOutputInfo$unit[[1]]$name,
			number=1)
	}
	MOfctr = trigMOfiles # how many files we've recorded details for so far

	for(b in 1:BenchmarkInfo$number){ # over each benchmark
		# Save info for 2nd (and maybe 3rd, 4th) MOs:
		MOunits[[(1+b)]] = BenchmarkInfo$unit[[b]]
		# Update the MO number, rather than reflecting the benchmark number
		MOunits[[(1+b)]]$number = 1 + b

		# Next part of files list are those files belonging to the benchmarks:
		for(f in 1:length(BenchmarkInfo$unit[[b]]$filepaths)){
			MOfctr = MOfctr + 1
			MOfiles[[MOfctr]] = list(path=BenchmarkInfo$unit[[b]]$filepaths[f],
				name=BenchmarkInfo$unit[[b]]$name,number=(b+1))

		}
	}

	MOnumber = 1 + BenchmarkInfo$number # Update the number of MOs
	MOnames = c(ModelOutputInfo$names[1],BenchmarkInfo$names) # Update the MO names

	# Write details to log file:
	writelog('Reduced analysed model outputs to triggering model output and its benchmarks only.', 
	         level = "warn",
	         LogSettings)



	return(list(files = MOfiles, unit = MOunits, number = MOnumber, nfiles = MOfctr, names = MOnames))
}
