# Spatial_EarthArea.R
#
# Gab Abramowitz UNSW 2020 (gabsun at gmail dot com)
#
# Calculates surface area of Earth and grid cells

EarthArea = function(obs,LogSettings){

	cellwidth =  obs$grid$lat[2] - obs$grid$lat[1] # assume n degree by n degree
	earthradius=6371
	earth = 4*pi*earthradius^2
	cell = matrix(NA,length(obs$grid$lon),length(obs$grid$lat))
	for(i in 1:length(obs$grid$lon)){
		for(j in 1:length(obs$grid$lat)){
			cell[i,j] = earthradius^2 * cellwidth/360*2*pi *
				(sin((obs$grid$lat[j]+cellwidth/2)/90*pi/2) - sin((obs$grid$lat[j]-cellwidth/2)/90*pi/2))
		}
	}

	if(abs(earth - sum(cell)) > 0.001){
		cat('Earth surface area issue..?!')
	}
	return(list(cell=cell,earth=earth))
}
