# Convert_Density.R
#
# Estimates the density of a 1D variable, removing QC-poor time steps
# if QC information is available
#
# Gab Abramowitz, UNSW, 2023 (gabsun at gmail dot com)

Convert_Density = function(data1D,var,forceqc, LogSettings){

  # Number of data we want before trying to calculate a density (defined in constants.R)
  minsample = density_minsample 
  # Number of bins in density estimates (defined in constants.R)   
  nbins=density_nbins 

  if(typeof(data1D$data) == 'character'){ # check this is actually a number variable
    result = NA
  }else if(length(data1D$data) < minsample){ # and that the sample size is big enough
    result = NA
  }else{
    # assuming this is not a characater variable, like veg_type, and that we have a large enough sample
    if(data1D$qcexists){ # there is Quality Control info for this variable, use it
      # Replace all time steps without a 'good QC' value with NA
      dataWQC = ifelse( (data1D$qc %in% data1D$goodqcvalue),data1D$data, NA)
      # Calculate density, excluding NAs, unless all are NA (in which case return NA)
      if(sum(!is.na(dataWQC))<minsample){
        result = NA
      }else{
        result =density(dataWQC,n=nbins,from=var$Range[1],to=var$Range[2],na.rm=TRUE)
      }
    }else if(forceqc$qcexists){ # else if we were requested to force to another qc info
      # Replace all time steps without a 'good QC' value with NA
      dataWQC = ifelse( (forceqc$qc %in% forceqc$goodqcvalue),data1D$data, NA)
      # Calculate density, excluding NAs, unless all are NA (in which case return NA)
      if(sum(!is.na(dataWQC))<minsample){
        result = NA
      }else{
        result =density(dataWQC,n=nbins,from=var$Range[1],to=var$Range[2],na.rm=TRUE)
      }
    }else{
      # Calculate density, excluding NAs, unless all are NA (in which case return NA)
      if(sum(!is.na(data1D$data))<minsample){
        result = NA
      }else{
        result =density(data1D$data,n=nbins,from=var$Range[1],to=var$Range[2],na.rm=TRUE)
      }
    }
  }
  return(result)
}
