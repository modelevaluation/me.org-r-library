# Plot_MetricTableBestInMetric.R
#
# Gab Abramowitz UNSW 2020 (gabsun at gmail dot com)
#
# Returns an integer that indicates whether the submitting LSM (1),
# the first (2), second (3), or third (4) nominated benchmark was
# best in a particular metric.

MetricTableBestInMetric = function(name,model,bench1,bench2,bench3,LogSettings){

	# If there are no benchmark values, or no metric value is given, there is no winner and 0 is returned
	if((is.na(bench1)&is.na(bench2)&is.na(bench3)) | grepl('nometric',tolower(name))){
		return(0)
	}
	if(grepl('nme',tolower(name)) | grepl('rmse',tolower(name))){
		result = which.min(c(model,bench1,bench2,bench3))
	}else if(grepl('grad',tolower(name)) | grepl('gradient',tolower(name))){
		result = which.min(abs(1-c(model,bench1,bench2,bench3)))
	}else if(grepl('pdf',tolower(name)) | grepl('overlap',tolower(name)) |
		grepl('correlation',tolower(name)) | grepl('cor',tolower(name))){
		result = which.max(c(model,bench1,bench2,bench3))
	}else if(grepl('int',tolower(name)) | grepl('intercept',tolower(name))){
		result = which.min(abs(c(model,bench1,bench2,bench3)))
	}else if(grepl('bias',tolower(name))){
		result = which.min(abs(c(model,bench1,bench2,bench3)))
	}
	return(result)
}
