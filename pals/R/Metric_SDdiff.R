# Metric_SDdiff.R
#
# Gab Abramowitz UNSW 2020 (gabsun at gmail dot com)
#

SDdiff = function(model,obs, LogSettings){
	# Create obs mask to make sure SD is calculated over the same time steps for obs and MO
	obsNotNAmask = !is.na(as.vector(obs))
	SDdiff = abs(1 - (sd(as.vector(model)[obsNotNAmask])/sd(as.vector(obs)[obsNotNAmask])))
	return(SDdiff)
}
