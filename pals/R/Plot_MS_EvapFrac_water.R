# Plot_MS_EvapFrac_water.R
#
# Gab Abramowitz UNSW 2025 (help at modelevaluation dot org)
#
Plot_MS_EvapFrac_water = function(AllObs,AllMods,AllObsSubsetV,modsiteidx,configidx=NULL, vars,
	subsetvars,AnalysisName,plottype='null', LogSettings){
	# Uses Rainf and Qle (simply the mean for each site), to plot the error in Qle/Rainf
	# for each model, as a box plot over all sites.

	thisfunction = 'Plot_MS_EvapFrac_water.R' # used for log file error reporting
    writelog(' Drawing water evaporative fraction plot...', level = "info", LogSettings)
	plotclass = 'summative' # used to direct analysis display on meorg
	shorttext = 'Water evaporative fraction' # for mouse hover in analysis display
	longtext = paste('Shows boxplots of the spread of the mean water evaporative fraction', 
		'across all sites involved in this analysis. This is calculated at each site using',
		'mean(evapotranspiration) / mean(precipitation), and the boxplot shows the spread',
		'across sites. If applyQC = TRUE in the Analysis Script for this Experiment, then',
		'time steps that have either of these variables gap-filled are excluded from the means.') # explanation text

	# First, check we have required variables for water evaporative fraction, Qle and Rainf,
    # and note their index in the list of loaded variables:
    foundRainf = FALSE
    foundQle = FALSE
    RainfIDX = NA
    QleIDX = NA
	for(sv in 1:length(subsetvars)){
		if(subsetvars[[sv]]$Name[1]=='Rainf'){
            foundRainf = TRUE
            RainfIDX = sv
		}
	}
    for(v in 1:length(vars)){
       	if((vars[[v]]$Name[1]=='Qle') | (vars[[v]]$Name[1]=='Qle_cor')){
            foundQle = TRUE
            QleIDX = v
        }
    }
	if(! (foundRainf & foundQle)){
		message = 'Plot_MS_EvapFrac_water called, but Rainf and Qle have not been requested! \n'
        cat(message)
        writelog(message, level = "warn", LogSettings)
		return(list(type=AnalysisName,filename=filepath,error=message,value=NA,err=FALSE,mimetype="image/png"))
	}

	# Create plot filename:
	filepath = paste(getwd(),setOutput('ModelAnalysis', LogSettings = LogSettings),sep = "/")

	nsites = length(modsiteidx[,1]) # number of sites
	nvars = length(vars) # number of variables
	nmodels = length(modsiteidx[1,]) # number of models

	# Calculate mean over all sites:
	evapfrac = matrix(NA,nsites,(1+nmodels))
	evapfracerr = matrix(NA,nsites,(nmodels))
	sitesfailed = c(1:(nmodels+1)) * 0 # initialise sites failed for obs + all models at 0

	# Calculate evaporative fraction for obs and models:
	for(s in 1:nsites){
		# First check there's good data at this site:
		if(is.na(AllObs[[s]][[QleIDX]]$Mean) | is.na(AllObsSubsetV[[s]][[RainfIDX]]$Mean) ){
			sitesfailed[1] = sitesfailed[1] + 1
		}else{
			# Find observed evaporative fraction:
			# First find average rainfall total (rather than rate) over a timestep, in mm or kg/m2
			AvRainfTotalPerTimestep = AllObsSubsetV[[s]][[RainfIDX]]$Mean * AllObs[[s]][[RainfIDX]]$timing$tstepsize
			# then the average latent heat flux, in mm or kg/m2 over a time step:
			AvQleInMM = AllObs[[s]][[QleIDX]]$Mean / 2256000 * AllObs[[s]][[RainfIDX]]$timing$tstepsize
			evapfrac[s,1] = AvQleInMM / AvRainfTotalPerTimestep
			# ... and the same for each model:
			for(m in 1:nmodels){
				if(is.na(AllMods[[ modsiteidx[s,m] ]][[QleIDX]]$Mean) ){
					sitesfailed[m+1] = sitesfailed[m+1] + 1
				}else{
					AvQleInMM = AllMods[[ modsiteidx[s,m] ]][[QleIDX]]$Mean / 2256000 * AllObs[[s]][[RainfIDX]]$timing$tstepsize
					evapfrac[s,(m+1)] = AvQleInMM / AvRainfTotalPerTimestep
					evapfracerr[s,m] = evapfrac[s,(m+1)] - evapfrac[s,1]
				}
			}
		}
	}

	# Save obs/model names for legend
	modelnames = c()
	for(m in 1:nmodels){modelnames[m] = AllMods[[ modsiteidx[1,m] ]]$name}

	# Find number of failed sites per model (based on var that failed the most)
	notfailedsites = c()
	notfailedmodels = c()
	notfailedsites[1] = nsites - sitesfailed[1]
	for(m in 1:nmodels){
		notfailedsites[(m+1)] = nsites - sitesfailed[(m+1)]
		notfailedmodels[m] = (notfailedsites[(m+1)]!=0)
	}

	cexa = 1 # axis text size contraction factor
	if(nmodels>10){cexa = 0.9}
	if(nmodels>20){cexa = 0.8}

	par(mar=c(10,4,4,2),cex.axis=cexa,las=2)
	
	cols = c()
	# If configidx available, use the DEV version of the plot
	if(!is.null(configidx)){
	  
	  # Work out dimensions of configidx
	  nconfig = dim(configidx)[1]
	  nbranch = dim(configidx)[2]
	  
	  # This loop creates a colour gradient for different colours for each configuration set
	  config_col = matrix(nrow = nconfig, ncol = nbranch)
	  for(c in 1:nconfig){
	    for(br in 1:nbranch){
	      config_central_col <- DEV_MS_colours()[c]
	      config_col[c,br] = paste0(config_central_col,round((br - 1)/(nbranch-1)*3 + 1))
	    }
	  }
	  
	  # LM_pos and BM_pos keep track of non-config LM models and benchmarks
	  LM_pos = nconfig + 1
	  BM_pos = 1
	  
	  # This loop samples colours from generated sets for nmodels
	  for(i in 1:nmodels){
	    array_pos = which(configidx == i, arr.ind = TRUE)
	    #If belonging to the configuration set
	    if(is.finite(array_pos[1])){
	      cols[i] = config_col[array_pos]
	      #If belonging to the benchmark set
	    } else if(modelnames[i] %in% BenchmarkInfo$names) {
	      cols[i] = DEV_MS_benchmark_colours()[BM_pos]
	      BM_pos = BM_pos + 1
	      #If belonging to the non-config LM set  
	    } else{
	      cols[i] = DEV_MS_colours()[LM_pos] 
	      LM_pos = LM_pos + 1
	    }
	  }
	  cols = adjustcolor(c("black", cols),alpha.f=0.8)
	} else {
	  cols = adjustcolor(P2colours(),alpha.f=0.8)[seq(1:(nmodels+1))]
	}
	
	boxplot(evapfracerr[,notfailedmodels],names=modelnames[notfailedmodels],
		ylab='Average evaporative fraction error [mm/mm] (zero is best)',
		outline=FALSE,range=1,main='Error in mean water evaporative fraction [Qle/Rainf] over sites',
		ylim=c(-0.5,0.7),col=cols[c(FALSE,notfailedmodels)])

	# Add grid to make values more readable:
	grid(lwd=2,col='grey85')
	abline(h=0,lwd=4,col='grey65')

	# Close the graphics device(s) to ensure plot(s) available
	graphics.off()
	
	return(list(list(type=AnalysisName,filename=filepath,value=NA,err=FALSE,
		shorttext=shorttext,longtext=longtext,plotclass=plotclass,mimetype="image/png")))

} # End function
