# Checks_CheckGrids.R
#
# Gab Abramowitz UNSW 2020 (gabsun at gmail dot com)
#
# Checks compatibility of observed and model output spatial grids

CheckGrids = function(mods1,mods2, LogSettings){

	errtext = 'ok'
	err = FALSE

	if( any(abs(mods1$grid$lat - mods2$grid$lat)>0.001) |
		any( abs(ifelse(mods1$grid$lon<0,mods1$grid$lon+360,mods1$grid$lon) -
		ifelse(mods2$grid$lon<0,mods2$grid$lon+360,mods2$grid$lon) )>0.001) ){
		err=TRUE
		errtext = paste(' !! Spatial grids of',mods1$name,'and',mods2$name,' are incompatible.')
	}
	result = list(err = err,errtext = errtext)
	return(result)
}
