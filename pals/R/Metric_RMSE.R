# Metric_RMSE.R
#
# Gab Abramowitz UNSW 2020 (gabsun at gmail dot com)
#
RMSE = function(model,obs, LogSettings){
	if(all(is.na(model)) | all(is.na(obs))){
		RMSE = NA
	}else{
		RMSE = sqrt(mean((as.vector(model)-as.vector(obs))^2, na.rm=TRUE))
	}
	return(RMSE)
}
