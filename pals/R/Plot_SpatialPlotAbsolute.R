# Plot_SpatialPlotAbsolute.R
#
# Gab Abramowitz UNSW 2020 (gabsun at gmail dot com)
#
# Layout of plots that show obs panel, model panel, model-obs panel and
# up to 3 benchmark-obs panels (i.e. max 6 panels).
# Density plots are always in the lower left, mean and SD values are either bottom
# right <= 4 panels, or top left for > 4 panels

SpatialPlotAbsolute = function(model,obs,bench,md,variable,plottype,region, LogSettings){

	errtext = 'ok'
	density_cut = 1/200 # truncate denisty plot x-axis at this fraction of max y value
	varname = variable[['Name']][1]
	unitstxt = variable[['UnitsText']]
	longvarname = variable[['PlotName']]
	npanels = bench$howmany + 3 # Number of map panels in plot

	# Plot layout:
	if(npanels <= 4){
		par(mfcol=c(2,2) ,mar=c(3,3,3,0.5),oma=c(0,0,0,1),mgp=c(1.8,0.5,0),ps=15,tcl=-0.4)
		density_location = DensityLocation(region,4, LogSettings = LogSettings)
		textloc = TextLocation(region,4, LogSettings = LogSettings)
	}else{
		par(mfcol=c(2,3) ,mar=c(3,3,3,0.5),oma=c(1,0,0.5,1),mgp=c(1.8,0.5,0),ps=18,tcl=-0.2)
		density_location = DensityLocation(region,6, LogSettings = LogSettings)
		textloc = TextLocation(region,6, LogSettings = LogSettings)
	}

	# Fetch colour scheme:
	zcols = ChooseColours(md$zrange,varname,'positive')
	diffcols = ChooseColours(md$diffrange,varname,'difference')

	### Draw plot panels ###
	# First plot: model
	title = paste(model$name,' ',longvarname,' ',plottype,sep='')
	errtext = GriddedPlot(region,obs$grid$lon,obs$grid$lat,md$modelm,mean(md$modelm,na.rm=T),sd(md$modelm,na.rm=T),
		varname,unitstxt,longvarname,md$zrange,zcols,title,textloc, LogSettings = LogSettings)
	# Second plot: obs
	title = paste(obs$name,' ',longvarname,' ',plottype,sep='')
	errtext = GriddedPlot(region,obs$grid$lon,obs$grid$lat,md$obsm,mean(md$obsm,na.rm=T),sd(md$obsm,na.rm=T),
		varname,unitstxt,longvarname,md$zrange,zcols,title,textloc, LogSettings = LogSettings)
	# Third plot: difference of model, obs
	title = paste('[',model$name,'-',obs$name,'] ',varname,' ',plottype,sep='')
	errtext = GriddedPlot(region,obs$grid$lon,obs$grid$lat,(md$modelm-md$obsm),mean((md$modelm-md$obsm),na.rm=T),
		sd((md$modelm-md$obsm),na.rm=T),varname,unitstxt,longvarname,md$diffrange,diffcols,title,textloc, LogSettings = LogSettings)
	# Plot benchmark obs differences that exist (plots 4-6):
	if(bench$exist){
		for(b in 1:bench$howmany){
			title = paste('[',bench[[ bench$index[b] ]]$name,'-',obs$name,'] ',varname,' ',plottype,sep='')
			errtext = GriddedPlot(region,obs$grid$lon,obs$grid$lat,(md$benchm[[b]] - md$obsm),mean((md$benchm[[b]]-md$obsm),na.rm=T),
				sd((md$benchm[[b]]-md$obsm),na.rm=T),varname,unitstxt,longvarname,md$diffrange,diffcols,title,textloc, LogSettings = LogSettings)
		}
	}

	### Add density insets ###
	mod_den = density(md$modelm,na.rm=TRUE) # calculate model density estimate
	obs_den = density(md$obsm,na.rm=TRUE) # calculate obs density estimate
	xrange = DensityXrange(list(mod_den,obs_den),density_cut, LogSettings = LogSettings)
	# Plot pdfs for model and obs
	in1 = InsetDensity(density_location[[1]],mod_den,xrange, LogSettings = LogSettings)
	in2 = InsetDensity(density_location[[2]],obs_den,xrange, LogSettings = LogSettings)
	moderr_den = density((md$modelm-md$obsm),na.rm=TRUE) # calculate model error density estimate
	xrange = DensityXrange(list(moderr_den),density_cut, LogSettings = LogSettings)
	if(bench$exist){
		bencherr_den = list()
		density_range_list = list(moderr_den)
		for(b in 1:bench$howmany){
			bencherr_den[[b]] = density((md$benchm[[b]]-md$obsm),na.rm=TRUE)
			density_range_list[[b+1]] = bencherr_den[[b]]
		}
		# If there's a benchmark, overwrite xrange to include benchmark info:
		xrange = DensityXrange(density_range_list,density_cut, LogSettings = LogSettings)
		for(b in 1:bench$howmany){
			inb = InsetDensity(density_location[[(b+3)]],bencherr_den[[b]],xrange, LogSettings = LogSettings)
		}
	}
	in3 = InsetDensity(density_location[[3]],moderr_den,xrange, LogSettings = LogSettings)

	result = list(errtext=errtext,err=FALSE,metrics=md$metrics)
	return(result)
}
