# Log_writelog.R
#
# Gab Abramowitz UNSW 2020 (gabsun at gmail dot com)
# Jon Cranko Page UNSW 2024 
#
# Write to log file

writelog <- function(text, level, LogSettings = list("filename" = "PALS.log", verbosity = "on")){
  
  filename <- LogSettings$filename
  verbosity <- LogSettings$verbosity
  
	if(tolower(verbosity) == "debug"){
	  write(paste0(toupper(level), ": ", text), file = filename, append = TRUE)
	} else if (tolower(level) %in% c("info", "warn", "error") && tolower(verbosity) == "on"){
	  write(paste0(toupper(level), ": ", text), file = filename, append = TRUE)
	} else if (tolower(level) %in% c("start", "end")){
	  write(paste0(toupper(level), ": ", text), file = filename, append = TRUE)
	} else {
	  # Do not log
	}
}
