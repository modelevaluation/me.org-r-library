# Load_DataMultiSite.R
#
# This function is typically called from Load_DS_MO.R
#
# Gab Abramowitz, UNSW, 2025 (gabsun at gmail dot com)

Load_DataMultiSite = function(variable,name,filepaths,prop,subsetidentifier,vindex, LogSettings){

  thisfunction = 'Load_DataMultiSite.R'

  # How close does lat/lon need to be to know we've identified the right site in a MO?
  site_match_threshold = 0.001 # default 0.001

  # First make sure we have been sent info about which file/s to target:
  if(is.na(subsetidentifier$type)){ # i.e. no subset / site specifiction info has been requested
    errtext = paste0('No subset identifier sent for ',variable[['Name']][1],
      ' in suspected multi-site experiment in Model Ouput: ', name,'. ',thisfunction)
    writelog(errtext, level = "error", LogSettings)
    return(list(err=TRUE,errtext=errtext,name=name))
  }

  # Try to find file with appropriate site data in it:
  matchindex = c() # indices of files in MO/DS that match this subset (e.g. appropriate site)
  matchcount = 0 # the number of files that match
  # If the subset identifier has a 'site'/'sitename' global attribute, retrieve it:
  refsitename = NA
  refsiteidx = ifelse(any(tolower(names(subsetidentifier$globalatts))=='site'),
    c(1:length(names(subsetidentifier$globalatts)))[tolower(names(subsetidentifier$globalatts))=='site'],NA)
  refsiteidx = ifelse(any(tolower(names(subsetidentifier$globalatts))=='sitename'),
    c(1:length(names(subsetidentifier$globalatts)))[tolower(names(subsetidentifier$globalatts))=='sitename'],refsiteidx)
  if(!is.na(refsiteidx)){refsitename = as.character(subsetidentifier$globalatts[[refsiteidx]])}

  for(f in 1:length(filepaths)){ # over each file in the MO
    # For this file, retrieve 'site'/'sitename' global attribute, if it exists:
    thissitename = NA
    thissiteidx = ifelse(any(tolower(names(prop$fglobalatts[[f]]))=='site'),
      c(1:length(names(prop$fglobalatts[[f]])))[tolower(names(prop$fglobalatts[[f]]))=='site'],NA)
    thissiteidx = ifelse(any(tolower(names(prop$fglobalatts[[f]]))=='sitename'),
      c(1:length(names(prop$fglobalatts[[f]])))[tolower(names(prop$fglobalatts[[f]]))=='sitename'],thissiteidx)
    if(!is.na(thissiteidx)){thissitename = as.character(prop$fglobalatts[[f]][[thissiteidx]])}

    # First look for files with matching sitename to subset identifier:
    if(!is.na(refsitename) & !is.na(thissitename) & (thissitename==refsitename)) {
      # in which case declare that a match has been found:
      matchcount = matchcount + 1
      matchindex[matchcount] = f
    # else look for lat/lon match to within tolerance:
    }else if( (is.na(refsitename)|is.na(thissitename)) & 
      (abs(prop$fgrids[[f]]$lat - subsetidentifier$grid$lat)<site_match_threshold) &
      ( abs(ifelse(prop$fgrids[[f]]$lon<0,prop$fgrids[[f]]$lon+360,prop$fgrids[[f]]$lon) -
      ifelse(subsetidentifier$grid$lon<0,subsetidentifier$grid$lon+360,subsetidentifier$grid$lon) )
      <site_match_threshold) ) {
      # in which case declare that a match has been found:
      matchcount = matchcount + 1
      matchindex[matchcount] = f

    # Could also look for a string match between site name and model filename or global data?
    }
  }
  # Check that some MO files found with subset identifier properties
  if(matchcount==0){
    errtext = paste0('Model Ouput: ', name,' and variable ',variable[['Name']][1],
      ' does not appear to have a file that matches site ',subsetidentifier$name,'. ',thisfunction)
    writelog(errtext, level = "error", LogSettings)
    return(list(err=TRUE,errtext=errtext,name=name))
  # ...and variable exists in files that matched subset identification proprties (e.g. in MO for correct site)
  }else if( length( intersect(matchindex,c(1:length(filepaths))[prop$vexists[,vindex]]) )== 0){
    errtext = paste0('Model Ouput: ', name,' and variable ',variable[['Name']][1],
      ' does not appear to have a file that matches site ',subsetidentifier$name,'. ',thisfunction)
    writelog(errtext, level = "error", LogSettings)
    return(list(err=TRUE,errtext=errtext,name=name))
  }

  # At this stage we have some files that match subset info AND contain the requested variable.
  # What's below assumes if that if there is more than one of these files, then they represent different time
  # periods that need to be stitched together, AND that each file contains the same number of time steps.

  # Note number of files to read:
  filestoread = intersect(matchindex,c(1:length(filepaths))[prop$vexists[,vindex]])
  nfilestoread = length(filestoread)

  # check that number of timesteps in each file is the same:
  if(!all(prop$tsteps[filestoread]==prop$tsteps[filestoread[1]])){
    errtext = paste('Model Ouput:', name,' and variable',variable[['Name']][1],
      ', site:',subsetidentifier$name,'. Cannot yet deal with case of multiple ',
      'file model output with different number of timesteps in each file.',thisfunction)
    writelog(errtext, level = "error", LogSettings)
    return(list(err=TRUE,errtext=errtext,name=name))
  }

  ntsteps = prop$tsteps[filestoread[1]] * nfilestoread # total number of time steps
  tsteps1 = prop$tsteps[filestoread[1]]

  # Allocate space for data:
  vdata = array(NA,dim=c(prop$fgrids[[filestoread[1]]]$lonlen,prop$fgrids[[filestoread[1]]]$latlen,ntsteps) )
  # Define the order to read files:
  fileorder = order(prop$year[filestoread])
  # Get data:
  for(f in 1:nfilestoread){ # For each file sent by js
    expectedlength = ntsteps * prop$fgrids[[ filestoread[fileorder[f]] ]]$lonlen *
      prop$fgrids[[ filestoread[fileorder[f]] ]]$latlen
    # Open file and read data:
    ncid = nc_open(filepaths[ filestoread[fileorder[f]] ],write=FALSE,readunlim=FALSE)
    vdata_tmp = ReadNcVar(ncid,prop$foundvarname[f,vindex],variable,expectedlength=expectedlength,
      globalatts=prop$fglobalatts[[ filestoread[fileorder[f]] ]], LogSettings = LogSettings)
    if(vdata_tmp$err){
      return(list(err=vdata_tmp$err,errtext=paste(vdata_tmp$errtext,'in',name),name=name))
    }

    var_dims = dim(vdata_tmp$data) # vector of variable dimension lengths
    # Netcdf IDs of these dimensions (actually not used for now):
    var_dim_ids = ncid$var[[ prop$foundvarname[f,vindex] ]]$dimids

    # Now write to data variable, making sure dimensions are appropriate:
    if(sum(var_dims>1) > 1){ # if there is more than one dimension that varies
        # i.e. if we have a variable that is more than 1D, not including spatial dims
        timedimlength = max(var_dims) # number of time steps
        # dim number of time dim - the only dim we'll keep below
        time_dim = c(1:length(var_dims))[var_dims==timedimlength] # dim number of time dim
        ##### FOR NOW WE"RE JUST AVERAGING TO ONE DIM ####
        vdata[,,((f-1)*tsteps1+1) : ((f-1)*tsteps1+tsteps1)] = apply(vdata_tmp$data,time_dim,mean)
    }else{
      vdata[,,((f-1)*tsteps1+1) : ((f-1)*tsteps1+tsteps1)] = vdata_tmp$data
    }
    nc_close(ncid)
  }

  # Create model timing list to reflect aggregated data:
  timingall = list(tstepsize=prop$ftiming[[filestoread[1]]]$tstepsize,tsteps=ntsteps,
    syear=prop$ftiming[[filestoread[1]]]$syear,smonth=prop$ftiming[[filestoread[1]]]$smonth,
    sday=prop$ftiming[[filestoread[1]]]$sday,stime=prop$ftiming[[filestoread[1]]]$stime,
    interval=prop$ftiming[[filestoread[1]]]$interval,tunits=prop$ftiming[[filestoread[1]]]$tunits,
    lontype=prop$lontype,whole=prop$ftiming[[filestoread[1]]]$whole,
    tstepinday=prop$ftiming[[filestoread[1]]]$tstepinday,
    lengthyears=prop$ftiming[[filestoread[1]]]$lengthyears)

  # Log details about this particular site / variable
  writelog(paste0(' -- ',timingall$tsteps,' time steps of size ',timingall$tstepsize,' ',
    timingall$tunits,' (',timingall$tstepinday,' in a day), starting ',timingall$stime,
    ' ',timingall$sday,'-',timingall$smonth,'-',timingall$syear),
    level = "info", LogSettings)

  return( list(timingall=timingall,vdata=vdata,err=FALSE,errtext=NA,
    grid=prop$fgrids[[filestoread[1]]],qc=NA,qcexists=FALSE,goodqcvalue=NA) )
}
