# Load_TimeUnitsCreate.R
#
# Gab Abramowitz UNSW 2020 (gabsun at gmail dot com)
#
# Determine data start date and time:

TimeUnitsCreate = function(starttime, LogSettings) {
	shour = floor(starttime$shod)
	smin = floor((starttime$shod - shour)*60)
	ssec = floor(((starttime$shod - shour)*60 - smin)*60)
	start_hod = paste(Create2Uchar(shour,LogSettings),':',Create2Uchar(smin,LogSettings),':',Create2Uchar(ssec,LogSettings),sep='')
	timeunits=paste('seconds since ',as.character(starttime$syear),'-',
		Create2Uchar(starttime$smonth,LogSettings),'-',Create2Uchar(starttime$sday,LogSettings),' ',
		start_hod,sep='')
	return(timeunits)
}
