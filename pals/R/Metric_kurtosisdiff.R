# Metric_kurtosisdiff.R
#
# Gab Abramowitz UNSW 2020 (gabsun at gmail dot com)
#
kurtosisdiff = function(model,obs,LogSettings){
	# Create obs mask to make sure kurtosis is calculated over the same time steps for obs and MO
	library(e1071)
	obsNotNAmask = !is.na(as.vector(obs))
	kurtosisdiff = abs(1 - (kurtosis(as.vector(model)[obsNotNAmask])/kurtosis(as.vector(obs)[obsNotNAmask])))
	return(kurtosisdiff)
}
