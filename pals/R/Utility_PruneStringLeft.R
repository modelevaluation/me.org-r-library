# Utility_PruneStringLeft.R
#
# Gab Abramowitz UNSW 2020 (gabsun at gmail dot com)
#
PruneStringLeft <- function(str,keepn,append=TRUE){
  # only prune if string is too long
  	if((nchar(str)>keepn) & append){# only add trailing dots if string is too long
  		tmp = substr(str, 1, keepn)
  		pruned = paste0(tmp,'...')
  	}else if(nchar(str)>keepn){
  		pruned = substr(str, 1, keepn)
  	}else{
  		pruned = str
  	}
}
