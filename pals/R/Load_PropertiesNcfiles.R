# Load_PropertiesNcfiles.R
#
# Gab Abramowitz UNSW 2021 (gabsun at gmail dot com)
#
# Loads timing, variable availability, grid info etc for a collection
# of nc files and variables in each file

PropertiesNcfiles = function(name,filepaths,vars,LogSettings){
	errtext=matrix('ok',length(filepaths),length(vars)) # initialise
	err=matrix(FALSE,length(filepaths),length(vars))
	ftiming = list() # initialise list of each file's timing details
	fgrids = list() # initialise list of each file's grid details
	vunits = list() # initialise list of each file's variable unit details
	fglobalatts = list()
	fexists = c() # does each file exist?
	vexists = matrix(FALSE,length(filepaths),length(vars)) # does requested variable exist in file (logical)?
	qcexists = matrix(FALSE,length(filepaths),length(vars)) # does qc variable exist in file (logical)?
	vindex = matrix(NA,length(filepaths),length(vars)) # index of variable if found
	foundvarname = matrix(NA,length(filepaths),length(vars)) # name of variable in file if found
	intervals = c() # time step interval in each file (text)
	tsteps = c() # number of time steps in each file
	year = c() # referece start year of each file
	time1 = c() # first time value in each file
	grids = c() # grid configuration (text) e.g. 'singlesite'
	lonmarker = c() # longitude values, just used to decide single/multi site
	lontype = c() # grid longitude type

	# First check that each file exists, which files contain the variables we want, that units are known,
	# and timing structure makes sense:
	for(f in 1:length(filepaths)){ # For each file
		# Check file exists:
		if(!file.exists(filepaths[f])){ # file doesn't exist
			errtext[f,] = paste('File',filepaths[f],'could not be found.')
			writelog(errtext[f,1], level = "warn", LogSettings)
			err[f,] = TRUE
			fexists[f]=FALSE
		}else{
			fexists[f]=TRUE
			# Get file ID for this file
			fid=nc_open(filepaths[f],write=FALSE,readunlim=FALSE)
			# Get timing details for this file:
			ftiming[[f]] = TimingNcfile(fid,LogSettings = LogSettings)
			if(ftiming[[f]]$err){ # If timing issue, record error
				errtext[f,] = ftiming[[f]]$errtext
				err[f,] = TRUE
				writelog(errtext[f,1], level = "warn", LogSettings)
			}else{ # timing seems okay
				# Store timing info for this file:
				intervals[f] = ftiming[[f]]$interval
				tsteps[f] = ftiming[[f]]$tsteps
				year[f] = ftiming[[f]]$syear
				time1[f] = ftiming[[f]]$time1
				# Get grid info for each file:
				fgrids[[f]] = SpatialGrid(fid,name, LogSettings = LogSettings)
				grids[f] = fgrids[[f]]$type # store grid type
				lontype[f] = fgrids[[f]]$lontype # store grid longitude type
				lonmarker[f] = fgrids[[f]]$lon[1] # store token longitude value
				if(fgrids[[f]]$err){	# if grid issue, record error
					errtext[f,] = errtext=grid$errtext
					err[f,] = TRUE
					writelog(errtext[f,1], level = "warn", LogSettings)
				}else{ # grid seems okay
					# Check whether any requested variables exist in this file:
					for (v in 1:length(vars)){
						exists = VarExistsAny(fid,vars[[v]][['Name']],standard_names=vars[[v]][['standard_name']], LogSettings = LogSettings)
						vexists[f,v] = exists$var
						qcexists[f,v] = exists$qc
						vindex[f,v] = exists$index
						foundvarname[f,v] = exists$foundvarname
						# Check variable units are known:
						if(exists$var){
							vunits[[v]] = UnitsExist(fid,foundvarname[f,v],vars[[v]],filepaths[f], LogSettings = LogSettings)
							# If units issue, return error:
							if(vunits[[v]]$err){
								errtext[f,v]=vunits[[v]]$errtext
								err[f,v]=TRUE
								writelog(errtext[f,v], level = "warn", LogSettings)
							}
						}
					} # over each variable
					# Get global attributes for this file:
					fglobalatts[[f]] = ncatt_get(fid,varid=0)
				} # grid seems okay?
			} # timing seems okay?
			nc_close(fid) # close this file
 		} # file exists?
	} # over each file
	properties = list(ftiming=ftiming,fgrids=fgrids,fexists=fexists,vexists=vexists,lonmarker=lonmarker,
		errtext=errtext,err=err,intervals=intervals,tsteps=tsteps,year=year,qcexists=qcexists,
		grids=grids,vunits=vunits,vindex=vindex,time1=time1,lontype=lontype,fglobalatts=fglobalatts,
		foundvarname=foundvarname,propexist=TRUE)
	return(properties)
}
