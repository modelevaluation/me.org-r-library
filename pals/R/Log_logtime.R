# Log_logtime.R
#
# Gab Abramowitz UNSW 2020 (gabsun at gmail dot com)
#
# Create line space and time stamp in log file

logtime <- function(level = "info", LogSettings){
		writelog(paste0('    Runtime: ',proc.time()[3],'s'),
		         level, 
		         LogSettings)
}
