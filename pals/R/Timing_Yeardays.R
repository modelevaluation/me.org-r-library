# Timing_Yeardays.R
#
# Gab Abramowitz UNSW 2023 (gabsun at gmail dot com)
#
# Returns an integer vector of the number of days in each year of a
# dataset, and whether it contains a whole number of years. Will cope with
# forcing leap and noleap calendars, and otherwise assume leap, unless
# assuming noleap means there is a whole number of years of data.
#
# startyear => integer starting year
# ndays => number of days in total
# calendar => force leap or noleap timing: 'noleap','leap' or 'noinfo'

Yeardays = function(startyear,ndays,calendar='noinfo',LogSettings) {

	# First, initialise our best guess about whether we're using a
	# leap calendar or not:
	if(calendar=='leap'){
		guessleap = TRUE # we know it's true because we've been told
	}else if(calendar=='noleap'){
		guessleap = FALSE # we know it's NOT true because we've been told
	}else{
		guessleap = TRUE # initialise before we potentially get more info
	}

	# Then start to add days to daysperyear vector:
	if(ndays<365){
		whole=FALSE # not a whole number of years of data
		daysperyear = c(ndays)
		years = c(startyear)
	}else{
		daysperyear = c() # days belonging to each year found
		ctr=0 # initialise
		year = startyear # initialise current year
		years = c() # initialise vector of integer years found
		days=ndays # initialise number of remaining days to attribute
		lpyrs = 0 # initialise number of leap years

		# Incrementally remove year of days from total number of days:
		repeat {
			ctr = ctr + 1
			years[ctr] = year
			if( is.leap(year,LogSettings = LogSettings) & calendar!='noleap'){
			# i.e. leap year and not explicitly noleap timing - this
			# means that 'noinfo' will for now be treated as though leap
				days = days - 366
				daysperyear[ctr] = 366
				lpyrs = lpyrs + 1
			}else{
					days = days - 365
					daysperyear[ctr] = 365
			}
			year = year + 1
			if(days<365){
				# If there days remaining and leap/noleap timing is certain,
				# in which case we know we've removed the correct number of days above:
				if( (days>0) & (calendar!='noinfo') ){
					daysperyear[ctr+1] = days
					years[ctr+1] = year
					whole=FALSE # not a whole number of years of data
				}else if( (days>0) & ( days!=(365-lpyrs) ) ){
				# Days are remaining and they're not equal the number we'd expect
				# if we'd assumed a leap calendar and were not using one:
					daysperyear[ctr+1] = days
					years[ctr+1] = year
					whole=FALSE # not a whole number of years of data
				}else if( (days>0) & (days==(365-lpyrs)) ){
				# Likely a non leap year data set, since the number of days remaining
				# is precisely additional leap days, in which case:
				# Set all years to have 365 days:
					daysperyear[1:(ctr+1)] = days
					years[ctr+1] = year
					whole=TRUE # mark as a whole number of years of data
					guessleap = FALSE # change our initial guess since it allows whole years
				}else{ # =0
					whole=TRUE
				}
				break
			}
		}
	}

	# Create return list:
	yeardays = list(daysperyear=daysperyear,whole=whole,years=years,leap=guessleap)
	return(yeardays)
}
