# Plot_MS_MAPMAT.R
#
# Gab Abramowitz UNSW 2025 (help at modelevaluation dot org)
#
Plot_MS_MAPMAT = function(AllObs,AllMods,AllObsSubsetV,modsiteidx,vars,
	subsetvars,AnalysisName,plottype='null', LogSettings){
	# Uses Rainf and Tair (simply the mean for each site) to create MAP-MAT plot

	thisfunction = 'Plot_MS_MAPMAT.R' # used for log file error reporting
    writelog(' Drawing MAPMAT plot...', level = "info", LogSettings)
	plotclass = 'summative' # used to direct analysis display on meorg
	shorttext = 'Sites\' mean temp and precip' # for mouse hover in analysis display

    nsites = length(modsiteidx[,1]) # number of sites
	nvars = length(vars) # number of variables
    nsubsetvars = length(subsetvars) # number of variables to subset results with
	nmodels = length(modsiteidx[1,]) # number of models

	# First, check we have required variables for water evaporative fraction, Qle and Rainf,
    # and note their index in the list of loaded variables:
    foundRainf = FALSE
    foundTair = FALSE
	foundVtype = FALSE
    RainfIDX = NA
    TairIDX = NA
	VtypeIDX = NA
	for(sv in 1:nsubsetvars){
		if(subsetvars[[sv]]$Name[1]=='Rainf'){
            foundRainf = TRUE
            RainfIDX = sv
		}else if(subsetvars[[sv]]$Name[1]=='Tair'){
            foundTair = TRUE
            TairIDX = sv
        }else if(subsetvars[[sv]]$Name[1]=='veg_type'){
			foundVtype = TRUE
            VtypeIDX = sv
		}
	}

	if(! (foundRainf & foundTair)){
		message = paste('Site MAP vs MAT called, but either Rainf or Tair have',
			'not been requested.',thisfunction)
        cat(message)
        writelog(message, level = "warn", LogSettings)
		return(list(type=AnalysisName,filename=NULL,error=message,
			value=NA,err=TRUE,mimetype="image/png"))
	}else if(! foundVtype){
		message = paste('Site MAP vs MAT called with vegt type colors, but veg_type',
			'has not been requested',thisfunction)
        cat(message)
        writelog(message, level = "warn", LogSettings)
		return(list(type=AnalysisName,filename=NULL,error=message,
			value=NA,err=TRUE,mimetype="image/png"))
	}

	# Create plot filename:
	filepath = paste(getwd(),setOutput('ModelAnalysis', LogSettings = LogSettings),sep = "/")

	# Calculate mean over all sites:
	AvRainf = c()
    AvTair = c()
    sitecode = c()
	vegtypes = c()
	sitesfailed = c() # Initialise
	siteyears = c()

	# Calculate evaporative fraction for obs and models, including Penman-Monteith model:
	for(s in 1:nsites){
		# First check there's good data at this site:
		if(is.na(AllObsSubsetV[[s]][[TairIDX]]$Mean) | is.na(AllObsSubsetV[[s]][[RainfIDX]]$Mean) ){
			sitesfailed[1] = sitesfailed[1] + 1
		}else{
			# First find average annual rainfall total (rather than rate) over a timestep, in mm or kg/m2
			AvRainf[s] = AllObsSubsetV[[s]][[RainfIDX]]$Mean * 
                AllObsSubsetV[[s]][[RainfIDX]]$timing$tstepsize * AllObsSubsetV[[s]][[RainfIDX]]$timing$tstepinday * 365
			AvTair[s] = AllObsSubsetV[[s]][[TairIDX]]$Mean - zeroC
            sitecode[s] = AllObsSubsetV[[s]][[TairIDX]]$globalatts$site_code
			# Write veg type of each site to vegtypes vector:
			vegtypes[s] = trimws(AllObsSubsetV[[s]][[VtypeIDX]]$data)
			# Record number of years for this site:
			siteyears[s] = AllObsSubsetV[[s]][[TairIDX]]$timing$lengthyears
		}
	}

	# Process veg_type info for colouring of dots:
	uniquevegtypes = unique(vegtypes) # vector of unique vegetation types
	ntypes = length(uniquevegtypes) # number of vegetation types
	startcolours = Lotsacolours()[1:ntypes]
	plotcols = c()
	for(s in 1:nsites){
		plotcols[s] = startcolours[uniquevegtypes == vegtypes[s]]
	}

	# Set point sizes of site dots depending years of data - max 3.5x inflation
	sitecex = siteyears / max(siteyears,na.rm=TRUE) * 3.5
	sitelengthrank = order(siteyears,decreasing=TRUE) # for plotting longer sites first

	# Save obs/model names for legend
	modelnames = c()
	for(m in 1:nmodels){modelnames[m] = AllMods[[ modsiteidx[1,m] ]]$name}

	# Find number of failed sites per model (based on var that failed the most)
	notfailedsites = c()
	notfailedsites[1] = nsites - sitesfailed[1]
	for(m in 1:nmodels){
		notfailedsites[(m+1)] = nsites - sitesfailed[(m+1)]
	}

    # Draw plot
	par(mar=c(4,4,4,2),las=2)
	plot(1,type='n',xlim=c(min(AvTair),max(AvTair)),ylim=c(min(AvRainf),min(max(AvRainf),4300)),
		ylab='Average annual precipitation [mm]',xlab='Annual average temperature [C]',main='MAP - MAT')
    text(x=AvTair,y=AvRainf,labels=sitecode,cex=0.5,pos=3,offset=0.65,col=plotcols)
    grid(lwd=2,col='grey85')
    points(x=AvTair[sitelengthrank],y=AvRainf[sitelengthrank],pch=20,col=plotcols[sitelengthrank],
		cex=sitecex[sitelengthrank])
	legend('topleft',legend=uniquevegtypes,col=startcolours,lty=0,pch=19,cex=0.9)

	# Close the graphics device(s) to ensure plot(s) available
	graphics.off()
	
	longtext = paste0('Shows the mean annual precipitation (MAP) and mean annual ', 
		'temperature (MAT) of all sites involved in this analysis. The size of dots ',
		'represents the record length at each site, in this case between',min(siteyears),
		' and ',max(siteyears),' years.') # explanation text

	return(list(list(type=AnalysisName,filename=filepath,value=NA,err=FALSE,
		shorttext=shorttext,longtext=longtext,mimetype="image/png")))

} # End function
