# Metric_TimeMeanAll.R
#
# Gab Abramowitz UNSW 2020 (gabsun at gmail dot com)
#
# Calculates time means for each grid point in obs, model and benchmark data
# for a gridded plot

TimeMeanAll = function(model,obs,bench,variable,plottype,cl, LogSettings){
	metrics = list()
	benchm = list()
	benchbias = c()
	# Calculate time means for plotting:
	modelm = TimeMean(model$data,cl, LogSettings)
	obsm = TimeMean(obs$data,cl, LogSettings) + modelm - modelm # to make sure ocean areas not included
	# Ranges of obs, model metric values (benchmarks only appear in difference plots):
	zrange = c(min(modelm,obsm,na.rm=TRUE),max(modelm,obsm,na.rm=TRUE))
	# Scalar metric for reporting:
	modelbias = mean(modelm-obsm,na.rm=TRUE)
	# Initial ranges for difference plots:
	dmax = max(modelm-obsm,na.rm=TRUE)
	dmin = min(modelm-obsm,na.rm=TRUE)
	if(bench$exist){
		for(b in 1:bench$howmany){
			# Get benchmark metric data, noting there may have been other benchmarks
			# that failed (so use bench$index)
			benchm[[b]] = TimeMean(bench[[ bench$index[b] ]]$data,cl, LogSettings)
			benchbias[b] = mean(benchm[[b]]-obsm,na.rm=TRUE)
			dmax = max(dmax,(benchm[[b]]-obsm),na.rm=TRUE)
			dmin = min(dmin,(benchm[[b]]-obsm),na.rm=TRUE)
		}
	}
	metrics[[1]] = list(name='TimeSpaceBias',model_value=modelbias,bench_value=benchbias)
	result = list(modelm = modelm, obsm=obsm, benchm=benchm, diffrange = c(dmin, dmax),
		zrange=zrange,metrics=metrics)
	return(result)
}
