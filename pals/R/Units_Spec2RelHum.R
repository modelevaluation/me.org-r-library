# Units_Spec2RelHum.R
#
# Gab Abramowitz UNSW 2020 (gabsun at gmail dot com)
#
# Specific to relative humidity conversion.
# Note that zeroC is in Constants.R

Spec2RelHum = function(specHum,tk,PSurf){
	# Converts relative humidity to specific humidity.
	# tk - T in Kelvin; PSurf in Pa; relHum as %
	tempC = tk - zeroC
	# Sat vapour pressure in Pa
	esat = 610.78*exp( 17.27*tempC / (tempC + 237.3) )
	# Then specific humidity at saturation:
	ws = 0.622*esat/(PSurf - esat)
	# Then relative humidity:
	relHum = pmax(pmin(specHum/ws*100, 100),0)

	return(relHum)
}
