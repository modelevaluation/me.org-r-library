# Plot_InsetDensity.R
#
# Gab Abramowitz UNSW 2020 (gabsun at gmail dot com)
#
# Adds an inset density plot

InsetDensity = function(location,densitydata,xrange,LogSettings){
	par(fig=location,new=T)
	plot(densitydata,lwd=3,main='',ylab='',xlab='',cex.axis=0.8,bty='n',mgp=c(2,0,0),yaxt='n',xlim=xrange,tcl=-0.2)
}
