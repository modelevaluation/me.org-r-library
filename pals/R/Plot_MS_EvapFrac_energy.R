# Plot_MS_EvapFrac_energy.R
#
# Gab Abramowitz UNSW 2025 (help at modelevaluation dot org)
#
Plot_MS_EvapFrac_energy = function(AllObs,AllMods,modsiteidx,configidx= NULL,vars,AnalysisName,plottype='null', LogSettings){
	# Uses Qh and Qle (simply the mean for each site), to plot the error in Qle/(Qle+Qh)
	# for each model, as a box plot over all sites.

	thisfunction = 'Plot_MS_EvapFrac_energy.R' # used for log file error reporting
    writelog(' Drawing energy evaporative fraction plot...', level = "info", LogSettings)
	plotclass = 'summative' # used to direct analysis display on meorg
	shorttext = 'Energy evaporative fraction' # for mouse hover in analysis display
	longtext = paste('Shows boxplots of the spread of the mean energy evaporative fraction', 
		'across all sites involved in this analysis. This is calculated at each site using',
		'mean(latent heat flux) / (mean(latent heat flux) + mean(sensible heat flux)), and the boxplot shows the spread',
		'across sites. If applyQC = TRUE in the Analysis Script for this Experiment, then',
		'time steps that have either of these variables gap-filled are excluded from the means.') # explanation text

	# First, check we have required variables for energy evaporative fraction, Qle and Qh,
    # and note their index in the list of loaded variables:
    foundQh = FALSE
    foundQle = FALSE
    QleIDX = NA
    QhIDX = NA
    for(v in 1:length(vars)){
        if((vars[[v]]$Name[1]=='Qh') | (vars[[v]]$Name[1]=='Qh_cor')){
            foundQh = TRUE
            QhIDX = v
        }else if((vars[[v]]$Name[1]=='Qle') | (vars[[v]]$Name[1]=='Qle_cor')){
            foundQle = TRUE
            QleIDX = v
        }
    }
	if(! (foundQh & foundQle)){
		message = 'Plot_MS_EvapFrac_energy called, but Qle and Qh have not been requested! \n'
        cat(message)
        writelog(message, level = "warn", LogSettings)
		return(list(type=AnalysisName,filename=filepath,error=message,value=NA,err=FALSE,mimetype="image/png"))
	}

    # Create plot filename:
	filepath = paste(getwd(),setOutput('ModelAnalysis', LogSettings = LogSettings),sep = "/")

	nsites = length(modsiteidx[,1]) # number of sites
	nvars = length(vars) # number of variables
	nmodels = length(modsiteidx[1,]) # number of models

	# Calculate mean over all sites:
	evapfrac = matrix(NA,nsites,(1+nmodels))
	evapfracerr = matrix(NA,nsites,(nmodels))
	sitesfailed = c(1:(nmodels+1)) * 0 # initialise sites failed for obs + all models at 0

	# Calculate evaporative fraction for obs and models:
	for(s in 1:nsites){
		# First check there's good data at this site:
		if(is.na(AllObs[[s]][[QleIDX]]$Mean) | is.na(AllObs[[s]][[QhIDX]]$Mean) ){
			sitesfailed[1] = sitesfailed[1] + 1
		}else{
			# Find observed evaporative fraction:
			evapfrac[s,1] = AllObs[[s]][[QleIDX]]$Mean / (AllObs[[s]][[QleIDX]]$Mean + AllObs[[s]][[QhIDX]]$Mean)
			# ... and the same for each model:
			for(m in 1:nmodels){
				if(is.na(AllMods[[ modsiteidx[s,m] ]][[QleIDX]]$Mean) | is.na(AllMods[[ modsiteidx[s,m] ]][[QhIDX]]$Mean) ){
					sitesfailed[m+1] = sitesfailed[m+1] + 1
				}else{
					evapfrac[s,(m+1)] = AllMods[[ modsiteidx[s,m] ]][[QleIDX]]$Mean /
						(AllMods[[ modsiteidx[s,m] ]][[QleIDX]]$Mean + AllMods[[ modsiteidx[s,m] ]][[QhIDX]]$Mean)
					evapfracerr[s,m] = evapfrac[s,(m+1)] - evapfrac[s,1]
				}
			}
		}
	}

	# Save obs/model names for legend
	modelnames = c()
	for(m in 1:nmodels){modelnames[m] = AllMods[[ modsiteidx[1,m] ]]$name}

	# Find number of failed sites per model (based on var that failed the most)
	notfailedsites = c()
	notfailedmodels = c()
	notfailedsites[1] = nsites - sitesfailed[1]
	for(m in 1:nmodels){
		notfailedsites[(m+1)] = nsites - sitesfailed[(m+1)]
		notfailedmodels[m] = (notfailedsites[(m+1)]!=0)
	}

	cexa = 1 # axis text size contraction factor
	if(nmodels>10){cexa = 0.9}
	if(nmodels>20){cexa = 0.8}

	par(mar=c(10,4,4,2),cex.axis=cexa,las=2)
	
	# If configidx available, use the DEV version of the plot
	cols = c()

	if(!is.null(configidx)){
	  
	  # Work out dimensions of configidx
	  nconfig = dim(configidx)[1]
	  nbranch = dim(configidx)[2]
	  
	  # This loop creates a colour gradient for different colours for each configuration set
	  config_col = matrix(nrow = nconfig, ncol = nbranch)
	  for(c in 1:nconfig){
	    for(br in 1:nbranch){
	      config_central_col <- DEV_MS_colours()[c]
	      config_col[c,br] = paste0(config_central_col,round((br - 1)/(nbranch-1)*3 + 1))
	    }
	  }
	  
	  # LM_pos and BM_pos keep track of non-config LM models and benchmarks
	  LM_pos = nconfig + 1
	  BM_pos = 1
	  
	  # This loop samples colours from generated sets for nmodels
	  for(i in 1:nmodels){
	    array_pos = which(configidx == i, arr.ind = TRUE)
	    #If belonging to the configuration set
	    if(is.finite(array_pos[1])){
	      cols[i] = config_col[array_pos]
	      #If belonging to the benchmark set
	    } else if(modelnames[i] %in% BenchmarkInfo$names) {
	      cols[i] = DEV_MS_benchmark_colours()[BM_pos]
	      BM_pos = BM_pos + 1
	      #If belonging to the non-config LM set  
	    } else{
	      cols[i] = DEV_MS_colours()[LM_pos] 
	      LM_pos = LM_pos + 1
	    }
	  }
	  cols = adjustcolor(c("black", cols),alpha.f=0.8)
	} else {
	  cols = adjustcolor(P2colours(),alpha.f=0.8)[seq(1:(nmodels+1))]
	}

	boxplot(evapfracerr[,notfailedmodels],names=modelnames[notfailedmodels],ylab='Average evaporative fraction error (zero is best)',
		outline=FALSE,range=1,main='Error in mean energy evaporative fraction [Qle/(Qle+Qh)] over sites',
		ylim=c(-0.5,0.4),col=cols[c(FALSE,notfailedmodels)])

	# Add grid to make values more readable:
	grid(lwd=2,col='grey85')
	abline(h=0,lwd=4,col='grey65')

	# Close the graphics device(s) to ensure plot(s) available
	graphics.off()
	
	return(list(list(type=AnalysisName,filename=filepath,value=NA,err=FALSE,
		shorttext=shorttext,longtext=longtext,plotclass=plotclass,mimetype="image/png")))

} # End function
