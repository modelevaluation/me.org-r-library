# Units_Mbar2Pa.R
#
# Gab Abramowitz UNSW 2020 (gabsun at gmail dot com)
#
# Converts air pressure in mbar to pa

Mbar2Pa = function(PSurf_mbar){
	PSurf_pa = PSurf_mbar * 100
	return(PSurf_pa)
}
