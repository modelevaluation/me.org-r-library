# Plot_VariableTables.R
#
# Gab Abramowitz UNSW 2020 (palshelp at gmail dot com)
#
# Plots tables of all requested variables, and the details of those
# found in the MO.

Plot_VariableTables = function(ModelData,vars, LogSettings){
	#
	variables_per_plot = 28
	nvars = length(vars) # number of variables to be plotted:
	nplots = ceiling(nvars/variables_per_plot)
	hspace=c(0,0.11,0.22,0.32,0.43,0.50,0.63,0.72) # spacing of columns in plot
	magfac=0.7
	vspacing = c(30:1) / 30
	results=list()
	nvars_remaining_to_plot = nvars
	for(p in 1:nplots){
		# Determine which variables go in this plot:
		start_var = (p-1)*variables_per_plot + 1
		nvars_in_this_plot = min(nvars_remaining_to_plot,variables_per_plot)
		outfile = setOutput('default',LogSettings = LogSettings) # Create outfilename
		par(mar=c(0,0,2,0)) # Create plot
		plot(0.5, ylim=c(0,1),xlim=c(0,1),type="n", axes=F, xlab="", ylab="")
		linectr = 0	# counter for number of lines written to plot

		# Add column titles as bold face text:
		linectr = linectr + 1
		text(hspace[1],vspacing[linectr],'Variable name',cex=magfac,pos=4,font=2)
		text(hspace[2],vspacing[linectr],'Found name',cex=magfac,pos=4,font=2)
		text(hspace[3],vspacing[linectr],'Units',cex=magfac,pos=4,font=2)
		text(hspace[4],vspacing[linectr],'Time step size',cex=magfac,pos=4,font=2)
		text(hspace[5],vspacing[linectr],'Time steps',cex=magfac,pos=4,font=2)
		text(hspace[6],vspacing[linectr],'Start time',cex=magfac,pos=4,font=2)
		text(hspace[7],vspacing[linectr],'Missing',cex=magfac,pos=4,font=2)
		text(hspace[8],vspacing[linectr],'Long name',cex=magfac,pos=4,font=2)

		# Add title:
		title(main=paste0('Variables found in ',ModelData[[1]]$name,', Page ',p))

		# Now write info for each variable:
		for(v in start_var:(start_var+nvars_in_this_plot-1) ){
			linectr = linectr + 1
			# Write requested variable name:
			text(hspace[1],vspacing[linectr],vars[[v]]$Name[1],cex=magfac,pos=4)
			if(any(ModelData[[v]]$err)){
				# Write error message if loading unsuccessful:
				text(hspace[2],vspacing[linectr],
					PruneStringLeft(ModelData[[v]]$errtext,130,append=TRUE),cex=magfac,pos=4)
			}else{
				# Write Found variable name
				text(hspace[2],vspacing[linectr],ModelData[[v]]$foundvarname[1],
					cex=magfac,pos=4)
				# Write found variable units:
				text(hspace[3],vspacing[linectr],ModelData[[v]]$vunits$value,cex=magfac,pos=4)
				# Write time step size:
				text(hspace[4],vspacing[linectr],paste0(signif(ModelData[[v]]$timing$tstepsize,digits=6),' ',
					ModelData[[v]]$timing$tunits),cex=magfac,pos=4)
				# Write number of time steps:
				text(hspace[5],vspacing[linectr],ModelData[[v]]$timing$tsteps,cex=magfac,pos=4)
				# Write start time:
				text(hspace[6],vspacing[linectr],paste0(round(ModelData[[v]]$timing$stime,4),'hr, ',
					ModelData[[v]]$timing$syear,'-',ModelData[[v]]$timing$smonth,'-',
					ModelData[[v]]$timing$sday),cex=magfac,pos=4)
				# Write percentage of data filled with fill value (i.e. not actual model output)
				if(is.null(ModelData[[v]]$vunits$fillvalue)){
					# No fill value exists
					text(hspace[7],vspacing[linectr],'No _FillValue',cex=magfac,pos=4)
				}else{
					# 'Bad' time steps are either filled values or those that are NA:
					filled_tsteps = sum(ModelData[[v]]$data==ModelData[[v]]$vunits$fillvalue,na.rm=TRUE) +
						 sum(is.na(ModelData[[v]]$data),na.rm=TRUE)
					percent_filled = round((filled_tsteps/ModelData[[v]]$timing$tsteps)*100,digits=0)
					text(hspace[7],vspacing[linectr],paste0(percent_filled,'%'),cex=magfac,pos=4)
				}
				# Write variable long name:
				text(hspace[8],vspacing[linectr],vars[[v]]$PlotName,cex=magfac,pos=4)

			}
		} # over each variable

		nvars_remaining_to_plot = nvars_remaining_to_plot - nvars_in_this_plot

		results[[p]] = list(type=paste0('Variables ',p),filename=paste(getwd(),outfile,sep = "/"),
				mimetype="image/png",metrics = NA,analysistype=paste0('Vars',p), variablename='all',
				bencherror='ok',obsname='all',moname='all',benchnames='all',analysesexist=TRUE)

	} # over each plot panel
	
	# Close the graphics device(s) to ensure plot(s) available
	graphics.off()
	
	return(results)
}
