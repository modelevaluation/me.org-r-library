# Metric_fifthdiff.R
#
# Gab Abramowitz UNSW 2020 (gabsun at gmail dot com)
#
fifthdiff = function(model,obs,LogSettings){
	# Create obs mask to make sure fifth percentile difference is calculated over the same time steps for obs and MO
	NotNAmask = ! ( is.na(as.vector(obs)) | is.na(as.vector(model)) )
	fifthdiff = abs(quantile(as.vector(model)[NotNAmask],c(0.05)) - quantile(as.vector(obs)[NotNAmask],c(0.05)))
	return(fifthdiff)
}
