# Convert_Mean.R
#
# Simply takes the average of a 1D field, removing QC-poor time steps
# if QC information is available
#
# Gab Abramowitz, UNSW, 2023 (gabsun at gmail dot com)

Convert_Mean = function(data1D,var,forceqc, LogSettings){
  if(typeof(data1D$data) != 'character'){ # assuming this is not a characater variable, like veg_type
    if(data1D$qcexists){ # there is Quality Control info for this variable, use it
      # Replace all time steps without a 'good QC' value with NA
      dataWQC = ifelse( (data1D$qc %in% data1D$goodqcvalue),data1D$data, NA)
      # Take mean, excluding NAs, unless all are NA (in which case return NA)
      result = ifelse( all(is.na(dataWQC)), NA, mean(dataWQC,na.rm=TRUE) )
    }else if(forceqc$qcexists){ # else if we were requested to force to another qc info
      # Replace all time steps without a 'good QC' value with NA
      dataWQC = ifelse( (forceqc$qc %in% forceqc$goodqcvalue),data1D$data, NA)
      # Take mean, excluding NAs, unless all are NA (in which case return NA)
      result = ifelse( all(is.na(dataWQC)), NA, mean(dataWQC,na.rm=TRUE) )
    }else{
      # Take mean, excluding NAs, unless all are NA (in which case return NA)
      result = ifelse( all(is.na(data1D$data)), NA, mean(data1D$data,na.rm=TRUE) )
    }
  }else{
    result = NA
  }
  return(result)
}
